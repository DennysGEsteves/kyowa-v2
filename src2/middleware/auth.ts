import { Request, Response, NextFunction } from 'express';
import * as jwt from 'jsonwebtoken';

const checkJwt = (req: Request, res: Response, next: NextFunction): void => {
  let token = String(req.cookies.access_token);

  if (!token) {
    res.status(401).json({ auth: false, message: 'No token provided.' });
  }

  token = token.replace('Bearer ', '');

  jwt.verify(token, process.env.JWT_SECRET, (err /* decoded: IUser */): any => {
    if (err) {
      res
        .status(401)
        .json({ auth: false, message: 'Failed to authenticate token.' });
    }

    // req.query. = decoded.email;

    res.header('Access-Control-Allow-Origin', 'http://localhost:3000');
    res.header('Access-Control-Allow-Credentials', 'true');
    res.header(
      'Access-Control-Allow-Methods',
      'GET, POST, PATCH, PUT, DELETE, OPTIONS',
    );
    res.header(
      'Access-Control-Allow-Headers',
      'Origin, X-Requested-With, Content-Type, Accept',
    );
    next();
  });
};

export default checkJwt;
