import { Router } from "express";

import checkJwt from "../middleware/auth";
import ArchitectRoutes from "./architects";
import AuthRoutes from "./auth";
import StoresRoutes from "./stores";
import UsersRoutes from "./users";

const router = Router();

router.use("/auth", AuthRoutes);
router.use(checkJwt);
router.use("/users", UsersRoutes);
router.use("/stores", StoresRoutes);
router.use("/architect", ArchitectRoutes);

export default router;
