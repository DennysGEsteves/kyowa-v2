import { writeFileSync } from 'fs';
import { IUser, IUserCreate } from 'src/types/User';

import * as UserRepository from '@repositories/UserRepository';

export const list = async (): Promise<IUser[]> => {
  const users = await UserRepository.list();

  return users.map((user) => {
    const tmp = { ...user };
    delete tmp.pass;
    return tmp;
  });
};

export const create = async (
  user: IUserCreate & { image?: any },
): Promise<IUser> => {
  const { image, ...data } = user;

  const newUser = await UserRepository.create(data);

  if (image) {
    const imageContent = image.replace(/^data:image\/(jpe?g|png);base64,/, '');
    writeFileSync(`public/img/users/${newUser.id}.jpg`, imageContent, 'base64');
  }

  delete newUser.pass;

  return newUser;
};

export const update = async (id: number, user: IUser): Promise<IUser> => {
  const { image, ...data } = user;
  if (image) {
    const imageContent = image.replace(/^data:image\/(jpe?g|png);base64,/, '');
    writeFileSync(`public/img/users/${id}.jpg`, imageContent, 'base64');
  }

  const updatedUser = await UserRepository.update(id, data);

  delete updatedUser.pass;

  return user;
};

export const remove = (id: number): Promise<IUser> => UserRepository.remove(id);

export const getByEmail = (email: string): Promise<IUser> =>
  UserRepository.getByEmail(email);
