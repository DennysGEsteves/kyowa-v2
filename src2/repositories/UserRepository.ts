import { User } from '@prisma/client';

import prisma from './prismaClient';

const { user: db } = prisma;

export const getByEmail = (email: string): Promise<User> =>
  db.findUnique({
    where: { email },
  });

export const create = (data: Omit<User, 'id' | 'pass'>): Promise<User> =>
  db.upsert({
    where: { email: data.email },
    create: data,
    update: data,
  });

export const update = (id: number, data: Omit<User, 'pass'>): Promise<User> =>
  db.update({
    where: { id },
    data,
  });

export const remove = (id: number): Promise<User> =>
  db.delete({
    where: { id },
  });

export const list = (): Promise<User[]> => db.findMany();

export const getByName = (name: string): Promise<User> =>
  db.findFirst({
    where: { name },
  });
