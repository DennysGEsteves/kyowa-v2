import { Application } from 'express';
import passport from 'passport';
import request from 'supertest';

import { ArchitectController } from '@core/controllers/architect';
import { UpsertArchitectDTO } from '@core/controllers/architect/dtos';
import { ResponseTypes } from '@infra/http/response-controller';
import { User as DBUser } from '@prisma/client';

import App from '../../../../../../src/app';
import {
  completeArchitectDataMock,
  completeUserDataMock,
} from '../../../../../mocks';

describe('ArchitectApi Endpoints', () => {
  let app: Application;

  beforeEach(async () => {
    app = App.app;

    const authenticatedUserMock = completeUserDataMock as DBUser;

    passport.authenticate = jest.fn((authType, options, callback) =>
      callback(null, authenticatedUserMock),
    );
  });

  describe('GET /api/architects', () => {
    it('should map route properly', async () => {
      jest
        .spyOn(ArchitectController.prototype, 'list')
        .mockImplementationOnce(async () => ResponseTypes.ok([]));

      const res = await request(app).get('/api/architects');

      expect(res.statusCode).toEqual(200);
      expect(res.body).toHaveProperty('payload');
      expect(res.body.payload).toEqual([]);
      expect(res.body).toHaveProperty('success');
    });
  });

  describe('POST /api/architects', () => {
    it('should map route properly', async () => {
      jest
        .spyOn(ArchitectController.prototype, 'upsert')
        .mockImplementationOnce(async () => ResponseTypes.created({}));

      const res = await request(app)
        .post('/api/architects')
        .send(completeArchitectDataMock as UpsertArchitectDTO);

      expect(res.statusCode).toEqual(201);
      expect(res.body).toHaveProperty('payload');
      expect(res.body.payload).toEqual({});
      expect(res.body).toHaveProperty('success');
    });
  });
});
