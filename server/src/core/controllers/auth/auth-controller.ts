import { AuthenticateUserDTO } from '@core/controllers/auth/dtos';
import { IAuthUseCases } from '@core/use-cases/auth/interfaces';
import { IResponseBody, ResponseTypes } from '@infra/http/response-controller';

export class AuthController {
  constructor(private authUseCases: IAuthUseCases) {}

  async authenticate(dto: AuthenticateUserDTO): Promise<IResponseBody> {
    const token = await this.authUseCases.authenticate(dto);
    return ResponseTypes.ok({ token });
  }
}
