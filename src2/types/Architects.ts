import { Architect } from '@prisma/client';

export type IArchitectCreate = Omit<Architect, 'id'>;
