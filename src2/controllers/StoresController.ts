import { Request, Response } from 'express';

import * as StoresService from '@services/StoresService';

export const list = async (req: Request, res: Response): Promise<void> => {
  try {
    const stores = await StoresService.list();
    res.json(stores);
  } catch (e) {
    console.log(e);
    res.status(500).send('Erro');
  }
};

export const create = async (req: Request, res: Response): Promise<void> => {
  try {
    const store = await StoresService.create(req.body);
    res.json(store);
  } catch (e) {
    console.log(e);
    res.status(500).send('Erro');
  }
};

export const update = async (req: Request, res: Response): Promise<void> => {
  const { userId } = req.params;

  try {
    const store = await StoresService.update(Number(userId), req.body);
    res.json(store);
  } catch (e) {
    console.log(e);
    res.status(500).send('Erro');
  }
};

export const remove = async (req: Request, res: Response): Promise<void> => {
  const { userId } = req.params;

  try {
    await StoresService.remove(Number(userId));
    res.send('ok');
  } catch (e) {
    console.log(e);
    res.status(500).send('Erro ');
  }
};
