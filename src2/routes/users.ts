import { Router } from 'express';

import * as UsersController from '@controllers/UsersController';

const router = Router();

router.get('/', UsersController.list);
router.post('/', UsersController.create);
router.get('/image/:userId', UsersController.getImage);
router.put('/:userId', UsersController.update);
router.delete('/:userId', UsersController.remove);

export default router;
