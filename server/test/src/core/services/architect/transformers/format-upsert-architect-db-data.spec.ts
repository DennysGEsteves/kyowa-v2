import { formatUpsertArchitectDBData } from '@core/services/architect';

import { completeArchitectDataMock } from '../../../../../mocks';

describe('FormatUpsertArchitectDBDataTransformer', () => {
  it('should return a DB Architect', async () => {
    // given

    // when
    const architect = formatUpsertArchitectDBData(completeArchitectDataMock);

    // then
    expect(architect).toEqual({
      email: architect.email,
      name: architect.name,
      cpf: architect.cpf,
      nasc: architect.nasc,
      active: architect.active,
      cep: architect.cep,
      address: architect.address,
      district: architect.district,
      city: architect.city,
      region: architect.region,
      phone1: architect.phone1,
      phone2: architect.phone2,
      obs: architect.obs,
      sellerId: architect.sellerId,
    });
  });
});
