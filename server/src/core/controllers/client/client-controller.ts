import { UpsertClientDTO } from '@core/controllers/client/dtos';
import { Client } from '@core/models/client';
import { IClientUseCases } from '@core/use-cases/client';
import { IResponseBody, ResponseTypes } from '@infra/http/response-controller';

export class ClientController {
  constructor(private clientUseCases: IClientUseCases) {}

  async list(): Promise<IResponseBody> {
    const clients = await this.clientUseCases.list();
    return ResponseTypes.ok(Client.toClientListPresenter(clients));
  }

  async upsert(dto: UpsertClientDTO): Promise<IResponseBody> {
    const client = await this.clientUseCases.upsert(dto);
    return ResponseTypes.created(Client.toUpsertClientPresenter(client));
  }
}
