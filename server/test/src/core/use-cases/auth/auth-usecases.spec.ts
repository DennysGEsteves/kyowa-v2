import bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';

import { AuthenticateUserDTO } from '@core/controllers/auth/dtos';
import { IUserService } from '@core/services/user';
import { AuthUseCases, IAuthUseCases } from '@core/use-cases/auth';
import { UnauthorizedException } from '@infra/http/exceptions/unauthorized-exception';

import { completeUserDataMock, UserServiceMock } from '../../../../mocks';

describe('AuthUseCases', () => {
  let authUseCases: IAuthUseCases;
  let userService: IUserService;

  beforeEach(async () => {
    userService = new UserServiceMock();
    authUseCases = new AuthUseCases(userService);
  });

  it('should be defined', async () => {
    expect(authUseCases).toBeDefined();
  });

  describe('authenticate', () => {
    it('should return authentication token if signin data is valid', async () => {
      // given
      process.env.JWT_SECRET = 'test secret';

      const authenticateUserDTOMock = {
        email: 'some email',
        pass: 'some pass',
      } as AuthenticateUserDTO;

      const findByEmailSpy = jest
        .spyOn(userService, 'findByEmail')
        .mockImplementationOnce(async () => completeUserDataMock);

      jest.spyOn(bcrypt, 'compareSync').mockImplementationOnce(() => true);
      jest.spyOn(jwt, 'sign').mockImplementationOnce(() => 'sometoken');

      // when
      const result = await authUseCases.authenticate(authenticateUserDTOMock);

      // then
      expect(findByEmailSpy).toBeCalledWith(authenticateUserDTOMock.email);
      expect(result).toBeTruthy();
      expect(result.length).toBeGreaterThan(15);
    });

    it('should throw error if user not found in signin email', async () => {
      process.env.JWT_SECRET = 'test secret';

      const authenticateUserDTOMock = {
        email: 'some email',
        pass: 'some pass',
      } as AuthenticateUserDTO;

      jest
        .spyOn(userService, 'findByEmail')
        .mockImplementationOnce(async () => undefined);

      // when
      const result = async () => {
        return authUseCases.authenticate(authenticateUserDTOMock);
      };

      await expect(result).rejects.toThrow(UnauthorizedException);
    });

    it('should throw error if signin pass is not valid', async () => {
      process.env.JWT_SECRET = 'test secret';

      const authenticateUserDTOMock = {
        email: 'some email',
        pass: 'some pass',
      } as AuthenticateUserDTO;

      jest
        .spyOn(userService, 'findByEmail')
        .mockImplementationOnce(async () => completeUserDataMock);

      jest.spyOn(bcrypt, 'compareSync').mockImplementationOnce(() => false);

      // when
      const result = async () => {
        return authUseCases.authenticate(authenticateUserDTOMock);
      };

      await expect(result).rejects.toThrow(UnauthorizedException);
    });
  });
});
