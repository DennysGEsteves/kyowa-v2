import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class AuthenticateUserDTO {
  @IsNotEmpty()
  @IsString()
  readonly pass: string;

  @IsNotEmpty()
  @IsEmail()
  readonly email: string;
}
