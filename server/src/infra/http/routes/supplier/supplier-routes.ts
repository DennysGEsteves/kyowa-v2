import { Request, Response, Router } from 'express';

import { UpsertSupplierDTO } from '@core/controllers/supplier/dtos';
import { supplierFactoryController } from '@core/factories/controller';
import { validateDto } from '@infra/http/middlewares/dto-validator';
import { ResponseController } from '@infra/http/response-controller';

class SupplierRoutes {
  router = Router();

  supplierController = supplierFactoryController();

  constructor() {
    this.list();
    this.upsert();
  }

  list() {
    this.router.get('/', async (req: Request, res: Response) => {
      return ResponseController.toJson(
        req,
        res,
        this.supplierController.list(),
      );
    });
  }

  upsert() {
    this.router.post(
      '/',
      validateDto(UpsertSupplierDTO),
      async (req: Request, res: Response) => {
        return ResponseController.toJson(
          req,
          res,
          this.supplierController.upsert({ ...req.body }),
        );
      },
    );
  }
}

export default new SupplierRoutes().router;
