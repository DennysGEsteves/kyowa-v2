import { Request, Response } from "express";
import { readFileSync } from "fs";

import * as ArchitectService from "@services/ArchitectService";

export const list = async (req: Request, res: Response): Promise<void> => {
  try {
    const data = await ArchitectService.list(req.body);
    res.json(data);
  } catch (e) {
    console.log(e);
    res.status(500).send("Erro");
  }
};

export const create = async (req: Request, res: Response): Promise<void> => {
  try {
    const newArchitectData = req.body;
    const architect = await ArchitectService.create(newArchitectData);
    res.json(architect);
  } catch (e) {
    console.log(e);
    res.status(500).send("Erro");
  }
};

export const update = async (req: Request, res: Response): Promise<void> => {
  const { architectId } = req.params;
  const dataArchitect = req.body;

  try {
    const architect = await ArchitectService.update(
      Number(architectId),
      dataArchitect
    );
    res.json(architect);
  } catch (e) {
    console.log(e);
    res.status(500).send("Erro");
  }
};

export const remove = async (req: Request, res: Response): Promise<void> => {
  const { architectId } = req.params;

  try {
    await ArchitectService.remove(Number(architectId));
    res.send("ok");
  } catch (e) {
    console.log(e);
    res.status(500).send("Erro");
  }
};

export const getImage = async (req: Request, res: Response): Promise<void> => {
  const { architectId } = req.params;
  let img;
  try {
    img = readFileSync(`public/img/users/${architectId}.jpg`);
  } catch (error) {
    img = readFileSync("public/img/users/avatar.jpg");
  }

  res.writeHead(200, { "Content-Type": "image/jpeg" });
  res.end(img, "binary");
};
