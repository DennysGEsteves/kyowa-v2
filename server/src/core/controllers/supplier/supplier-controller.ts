import { UpsertSupplierDTO } from '@core/controllers/supplier/dtos';
import { Supplier } from '@core/models/supplier';
import { ISupplierUseCases } from '@core/use-cases/supplier';
import { IResponseBody, ResponseTypes } from '@infra/http/response-controller';

export class SupplierController {
  constructor(private supplierUseCases: ISupplierUseCases) {}

  async list(): Promise<IResponseBody> {
    const suppliers = await this.supplierUseCases.list();
    return ResponseTypes.ok(Supplier.toSuppliersListPresenter(suppliers));
  }

  async upsert(dto: UpsertSupplierDTO): Promise<IResponseBody> {
    const supplier = await this.supplierUseCases.upsert(dto);
    return ResponseTypes.created(Supplier.toUpsertSupplierPresenter(supplier));
  }
}
