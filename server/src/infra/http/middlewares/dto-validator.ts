import { transformAndValidate } from 'class-transformer-validator';
import { Request, Response, NextFunction } from 'express';

function validateDto<T>(c: T) {
  return function ExpressClassValidate(
    req: Request,
    res: Response,
    next: NextFunction,
  ): any {
    const toValidate = req.body;
    if (!toValidate) {
      res.status(400).json({
        payload: { error: 'No request body found' },
        success: false,
      });
    } else {
      transformAndValidate(c as any, toValidate, {
        validator: { whitelist: true },
      })
        .then((transformed) => {
          req.body = transformed;
          next();
        })
        .catch((err) => {
          res.status(400).json({
            payload: {
              error: 'Validation failed',
              message: err,
            },
            success: false,
          });
        });
    }
  };
}

export { validateDto };
