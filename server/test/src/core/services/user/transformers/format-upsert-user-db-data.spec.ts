import bcrypt from 'bcrypt';

import { User } from '@core/models/user';
import { formatUpsertUserDBData } from '@core/services/user';

describe('FormatUpsertUserDBDataTransformer', () => {
  it('should return a DB User', async () => {
    // given
    const userMock = {
      name: 'some name',
      email: 'some email',
      active: true,
      login: 'some login',
      pass: 'some pass',
      permission: 'admin',
      phone: 'some phone',
      storeId: 99999,
    } as User;

    const bcryptHashMock = 'some hash';

    jest.spyOn(bcrypt, 'hashSync').mockImplementationOnce(() => bcryptHashMock);

    const user = formatUpsertUserDBData(userMock);

    expect(user).toEqual({
      active: user.active,
      email: user.email,
      login: user.login,
      name: user.name,
      pass: bcryptHashMock,
      permission: user.permission,
      phone: user.phone,
      storeId: user.storeId,
    });
  });
});
