import { IAuthUseCases } from '@core/use-cases/auth';

export const AuthUseCasesMock = jest.fn().mockImplementation(() => {
  return {
    authenticate: jest.fn(),
  } as IAuthUseCases;
});
