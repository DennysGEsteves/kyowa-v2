import { IUserService } from '@core/services/user';

export const UserServiceMock = jest.fn().mockImplementation(() => {
  return {
    list: jest.fn(),
    upsert: jest.fn(),
    findByEmail: jest.fn(),
  } as IUserService;
});
