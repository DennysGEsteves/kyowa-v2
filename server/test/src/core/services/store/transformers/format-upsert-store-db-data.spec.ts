import { formatUpsertStoreDBData } from '@core/services/store';

import { completeStoreDataMock } from '../../../../../mocks';

describe('FormatUpsertStoreDBDataTransformer', () => {
  it('should return a DB Store', async () => {
    // given

    // when
    const store = formatUpsertStoreDBData(completeStoreDataMock);

    // then
    expect(store).toEqual({
      name: store.name,
      email: store.email,
      address: store.address,
      cep: store.cep,
      city: store.city,
      district: store.district,
      managerId: store.managerId,
      obs: store.obs,
      phone1: store.phone1,
      phone2: store.phone2,
      region: store.region,
    });
  });
});
