import { UpsertArchitectDTO } from '@core/controllers/architect/dtos';
import { Architect } from '@core/models/architect';
import { IArchitectService } from '@core/services/architect/';
import { IArchitectUseCases } from '@core/use-cases/architect';

export class ArchitectUseCases implements IArchitectUseCases {
  constructor(private architectService: IArchitectService) {}

  list(): Promise<Architect[]> {
    return this.architectService.list();
  }

  upsert(dto: UpsertArchitectDTO): Promise<Architect> {
    const architect = Architect.fromUpsertArchitectDTO(dto);
    return this.architectService.upsert(architect);
  }
}
