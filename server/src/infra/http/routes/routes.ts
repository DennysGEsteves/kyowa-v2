import { Router } from 'express';
import passport from 'passport';

import { persistentLogin } from '../middlewares/persistent-login';
import { ArchitectRoutes } from './architect';
import { AuthRoutes } from './auth';
import { ClientRoutes } from './client';
import { StoreRoutes } from './store';
import { SupplierRoutes } from './supplier';
import { UserRoutes } from './user';

class Routes {
  router = Router();

  constructor() {
    this.auth();
    this.users();
    this.stores();
    this.architects();
    this.clients();
    this.suppliers();
  }

  auth() {
    this.router.use('/auth', AuthRoutes);

    this.router.use(
      '/users',
      passport.authenticate('jwt', { session: false }),
      UserRoutes,
    );
    this.router.use(
      '/stores',
      passport.authenticate('jwt', { session: false }),
      StoreRoutes,
    );
    this.router.use(
      '/architects',
      passport.authenticate('jwt', { session: false }),
      ArchitectRoutes,
    );

    this.router.use(
      '/clients',
      passport.authenticate('jwt', { session: false }),
      ClientRoutes,
    );

    this.router.use(
      '/suppliers',
      passport.authenticate('jwt', { session: false }),
      SupplierRoutes,
    );
  }

  users() {
    this.router.use('/users', persistentLogin, UserRoutes);
  }

  stores() {
    this.router.use('/stores', persistentLogin, StoreRoutes);
  }

  architects() {
    this.router.use('/architects', persistentLogin, ArchitectRoutes);
  }

  clients() {
    this.router.use('/clients', persistentLogin, ArchitectRoutes);
  }

  suppliers() {
    this.router.use('/suppliers', persistentLogin, ArchitectRoutes);
  }
}

export default new Routes().router;
