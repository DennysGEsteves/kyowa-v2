import { UpsertUserDTO } from '@core/controllers/user/dtos';
import { User } from '@core/models/user';
import { IUserService } from '@core/services/user/';
import { IUserUseCases } from '@core/use-cases/user';

export class UserUseCases implements IUserUseCases {
  constructor(private userService: IUserService) {}

  list(): Promise<User[]> {
    return this.userService.list();
  }

  upsert(dto: UpsertUserDTO): Promise<User> {
    const user = User.fromUpsertUserDTO(dto);
    return this.userService.upsert(user);
  }

  remove(id: number): Promise<void> {
    return this.userService.remove(id);
  }
}
