import { IArchitectUseCases } from '@core/use-cases/architect';

export const ArchitectUseCasesMock = jest.fn().mockImplementation(() => {
  return {
    list: jest.fn(),
    upsert: jest.fn(),
  } as IArchitectUseCases;
});
