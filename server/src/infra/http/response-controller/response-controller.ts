import { Request, Response } from 'express';

import { User } from '@core/models/user';

import { UnauthorizedException } from '../exceptions/unauthorized-exception';
import { ResponseTypes } from './response-types';
import { IResponseBody } from './types';

export class ResponseController {
  static toJson = async (
    request: Request,
    response: Response,
    responseData: Promise<any>,
  ): Promise<void> => {
    let json: IResponseBody;

    const now = new Date().toISOString();
    console.log(
      `${now} ${JSON.stringify({
        originalUrl: request.originalUrl,
        method: request.method,
        headers: request.headers,
        params: request.params,
        body: request.body,
        user: User.toLoggerPresenter(response.locals.user),
      })}\n`,
    );

    try {
      json = await responseData;
    } catch (error) {
      if (error instanceof UnauthorizedException) {
        json = ResponseTypes.unauthorized(error);
      } else {
        json = ResponseTypes.fail(error);
      }
    }

    response.status(json.statusCode).json({
      success: json.success,
      payload: json.payload,
    });
  };
}
