import { Architect } from '@core/models/architect';

export interface IArchitectService {
  list: () => Promise<Architect[]>;
  upsert(architect: Architect): Promise<Architect>;
  findByEmail(email: string): Promise<Architect>;
}
