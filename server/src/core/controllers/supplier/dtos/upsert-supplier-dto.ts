import {
  IsEmail,
  IsNotEmpty,
  IsOptional,
  IsString,
  IsNumber,
} from 'class-validator';

export class UpsertSupplierDTO {
  @IsOptional()
  @IsNumber()
  readonly id?: number;

  @IsNotEmpty()
  @IsString()
  readonly name: string;

  @IsOptional()
  @IsString()
  readonly nameFilter?: string;

  @IsOptional()
  @IsString()
  readonly cpnj?: string;

  @IsOptional()
  @IsString()
  readonly im?: string;

  @IsOptional()
  @IsString()
  readonly ie?: string;

  @IsOptional()
  @IsEmail()
  readonly email?: string;

  @IsOptional()
  @IsString()
  readonly cep?: string;

  @IsOptional()
  @IsString()
  readonly address?: string;

  @IsOptional()
  @IsString()
  readonly district?: string;

  @IsOptional()
  @IsString()
  readonly city?: string;

  @IsOptional()
  @IsString()
  readonly region?: string;

  @IsOptional()
  @IsString()
  readonly phone1?: string;

  @IsOptional()
  @IsString()
  readonly phone2?: string;

  @IsOptional()
  @IsString()
  readonly obs?: string;
}
