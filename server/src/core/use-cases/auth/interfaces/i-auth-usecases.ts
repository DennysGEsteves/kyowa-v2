import { AuthenticateUserDTO } from '@core/controllers/auth/dtos';

export interface IAuthUseCases {
  authenticate(dto: AuthenticateUserDTO): Promise<string>;
}
