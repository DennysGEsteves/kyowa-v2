import { Store } from '@core/models/store';
import { formatUpsertStoreDBData, IStoreService } from '@core/services/store';
import { IStoreRepository } from '@infra/repositories/store';

export class StoreService implements IStoreService {
  constructor(private storeRepository: IStoreRepository) {}

  async list(): Promise<Store[]> {
    const stores = await this.storeRepository.list();
    return Store.fromStoreListDBResponse(stores);
  }

  async upsert(store: Store): Promise<Store> {
    const formatStore = formatUpsertStoreDBData(store);
    const upsertStore = await this.storeRepository.upsert(
      formatStore,
      store.id,
    );
    return Store.fromStoreDBResponse(upsertStore);
  }

  async findById(id: number): Promise<Store> {
    const store = await this.storeRepository.findById(id);
    return Store.fromStoreDBResponse(store);
  }
}
