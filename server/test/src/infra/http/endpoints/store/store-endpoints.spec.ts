import { Application } from 'express';
import passport from 'passport';
import request from 'supertest';

import { StoreController } from '@core/controllers/store';
import { UpsertStoreDTO } from '@core/controllers/store/dtos';
import { ResponseTypes } from '@infra/http/response-controller';
import { User as DBUser } from '@prisma/client';

import App from '../../../../../../src/app';
import {
  completeStoreDataMock,
  completeUserDataMock,
} from '../../../../../mocks';

describe('StoreApi Endpoints', () => {
  let app: Application;

  beforeEach(async () => {
    app = App.app;

    const authenticatedUserMock = completeUserDataMock as DBUser;

    passport.authenticate = jest.fn((authType, options, callback) =>
      callback(null, authenticatedUserMock),
    );
  });

  describe('GET /api/stores', () => {
    it('should map route properly', async () => {
      jest
        .spyOn(StoreController.prototype, 'list')
        .mockImplementationOnce(async () => ResponseTypes.ok([]));

      const res = await request(app).get('/api/stores');

      expect(res.statusCode).toEqual(200);
      expect(res.body).toHaveProperty('payload');
      expect(res.body.payload).toEqual([]);
      expect(res.body).toHaveProperty('success');
    });
  });

  describe('POST /api/stores', () => {
    it('should map route properly', async () => {
      jest
        .spyOn(StoreController.prototype, 'upsert')
        .mockImplementationOnce(async () => ResponseTypes.created({}));

      const res = await request(app)
        .post('/api/stores')
        .send(completeStoreDataMock as UpsertStoreDTO);

      expect(res.statusCode).toEqual(201);
      expect(res.body).toHaveProperty('payload');
      expect(res.body.payload).toEqual({});
      expect(res.body).toHaveProperty('success');
    });
  });
});
