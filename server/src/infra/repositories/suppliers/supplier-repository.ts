import { IFormatUpsertSupplierDBData } from '@core/services/supplier';
import prisma from '@infra/repositories/prismaClient';
import { ISupplierRepository } from '@infra/repositories/suppliers';
import { Supplier as DBSupplier } from '@prisma/client';

const { supplier: db } = prisma;

export class SupplierRepository implements ISupplierRepository {
  list(): Promise<DBSupplier[]> {
    return db.findMany();
  }

  upsert(
    supplier: IFormatUpsertSupplierDBData,
    id: number,
  ): Promise<DBSupplier> {
    return db.upsert({
      where: { id: id || 0 },
      create: supplier,
      update: supplier,
    });
  }
}
