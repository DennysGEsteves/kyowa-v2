import { IArchitectCreate } from "src/types/Architect";
import { FetchOptions, FetchResult } from "src/types/Table";

import { Architect } from "@prisma/client";

import prisma from "./prismaClient";

const { architect: db } = prisma;

export const getByEmail = (email: string): Promise<Architect> =>
  db.findUnique({
    where: { email },
  });

export const create = (data: IArchitectCreate): Promise<Architect> => {
  const { sellerId, ...rest } = data;

  return db.upsert({
    where: { email: rest.email },
    create: {
      ...rest,
      seller: {
        connect: {
          id: sellerId,
        },
      },
    },
    update: data,
  });
};

export const update = (id: number, data: Architect): Promise<Architect> =>
  db.update({
    where: { id },
    data,
  });

export const remove = (id: number): Promise<Architect> =>
  db.delete({
    where: { id },
  });

export const list = async (options: FetchOptions): Promise<FetchResult> => {
  const { page, perPage, search } = options;
  let where = {};

  if (search) {
    const searchObj = {
      contains: options.search,
      mode: "insensitive",
    };

    where = {
      OR: {
        name_filter: searchObj,
        cpf: searchObj,
        email: searchObj,
      },
      orderBy: {
        name_filter: "asc",
      },
    };
  }

  const res = await prisma.$transaction([
    db.count({
      where,
    }),
    db.findMany({
      where,
      skip: page * perPage,
      take: perPage,
    }),
  ]);

  return {
    totalRows: res[0],
    rows: res[1],
  };
};
