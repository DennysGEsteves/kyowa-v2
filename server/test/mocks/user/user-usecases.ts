import { IUserUseCases } from '@core/use-cases/user';

export const UserUseCasesMock = jest.fn().mockImplementation(() => {
  return {
    list: jest.fn(),
    upsert: jest.fn(),
  } as IUserUseCases;
});
