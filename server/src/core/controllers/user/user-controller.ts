import { UpsertUserDTO } from '@core/controllers/user/dtos';
import { User } from '@core/models/user';
import { IUserUseCases } from '@core/use-cases/user/interfaces';
import { IResponseBody, ResponseTypes } from '@infra/http/response-controller';

export class UserController {
  constructor(private userUseCases: IUserUseCases) {}

  async list(): Promise<IResponseBody> {
    const users = await this.userUseCases.list();
    return ResponseTypes.ok(User.toUsersListPresenter(users));
  }

  async upsert(dto: UpsertUserDTO): Promise<IResponseBody> {
    const user = await this.userUseCases.upsert(dto);
    return ResponseTypes.created(User.toUpsertUserPresenter(user));
  }

  async remove(id: number): Promise<IResponseBody> {
    return ResponseTypes.ok(this.userUseCases.remove(id));
  }
}
