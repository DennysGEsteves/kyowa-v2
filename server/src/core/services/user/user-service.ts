import { User } from '@core/models/user';
import { IUserService, formatUpsertUserDBData } from '@core/services/user';
import { IUserRepository } from '@infra/repositories/user';
import { User as DBUser } from '@prisma/client';

export class UserService implements IUserService {
  constructor(private userRepository: IUserRepository) {}

  async list(): Promise<User[]> {
    const users = await this.userRepository.list();
    return User.fromUserListDBResponse(users);
  }

  async upsert(user: User): Promise<User> {
    const formatUser = formatUpsertUserDBData(user);
    const upsertUser = await this.userRepository.upsert(formatUser);
    return User.fromUserDBResponse(upsertUser);
  }

  findByEmail(email: string): Promise<DBUser> {
    return this.userRepository.findByEmail(email);
  }

  async remove(id: number): Promise<void> {
    await this.userRepository.remove(id);
  }
}
