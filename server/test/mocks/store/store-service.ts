import { IStoreService } from '@core/services/store';

export const StoreServiceMock = jest.fn().mockImplementation(() => {
  return {
    findById: jest.fn(),
    list: jest.fn(),
    upsert: jest.fn(),
  } as IStoreService;
});
