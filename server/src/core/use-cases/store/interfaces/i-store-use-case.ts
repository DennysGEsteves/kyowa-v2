import { Store } from '@core/models/store';
import { UpsertStoreDTO } from '@core/controllers/store/dtos';

export interface IStoreUseCases {
  list: () => Promise<Store[]>;
  upsert(dto: UpsertStoreDTO): Promise<Store>;
}
