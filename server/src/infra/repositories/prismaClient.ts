import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

export const getNewIndex = async (db: any): Promise<number> => {
  const { _all: count } = await db.count({
    select: {
      _all: true,
    },
  });

  return count + 1;
};

prisma.$use(async (params, next) => {
  if (['upsert', 'create'].includes(params.action)) {
    const key = params.action === 'upsert' ? 'create' : 'data';

    params.args[key].id = await getNewIndex(
      prisma[params.model.toLocaleLowerCase()],
    );
  }

  const result = await next(params);
  return result;
});

export default prisma;
