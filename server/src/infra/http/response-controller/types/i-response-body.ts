export type IResponseBody = {
  statusCode: number;
  payload: any;
  success: boolean;
};
