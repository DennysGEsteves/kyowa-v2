import { StoreController } from '@core/controllers/store';
import { StoreService } from '@core/services/store/store-service';
import { StoreUseCases } from '@core/use-cases/store';
import { StoreRepository } from '@infra/repositories/store';

export const storeFactoryController = (): StoreController => {
  const repository = new StoreRepository();
  const service = new StoreService(repository);
  const useCases = new StoreUseCases(service);
  const controller = new StoreController(useCases);
  return controller;
};
