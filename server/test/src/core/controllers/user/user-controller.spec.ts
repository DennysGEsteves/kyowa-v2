import { UserController } from '@core/controllers/user';
import { UpsertUserDTO } from '@core/controllers/user/dtos';
import { User } from '@core/models/user';
import { IUserUseCases } from '@core/use-cases/user';

import {
  basicUserDataMock,
  completeUserDataMock,
  UserUseCasesMock,
} from '../../../../mocks';

describe('UserController', () => {
  let controller: UserController;
  let useCases: IUserUseCases;

  beforeEach(async () => {
    useCases = new UserUseCasesMock();
    controller = new UserController(useCases);
  });

  it('should be defined', async () => {
    expect(controller).toBeDefined();
  });

  describe('list', () => {
    it('should return a Users list', async () => {
      // given
      const expectedResult = {
        name: completeUserDataMock.name,
        email: completeUserDataMock.email,
        permission: completeUserDataMock.permission,
        storeId: completeUserDataMock.storeId,
        active: completeUserDataMock.active,
        login: completeUserDataMock.login,
        phone: completeUserDataMock.phone,
      };

      const userUseCasesListSpy = jest
        .spyOn(useCases, 'list')
        .mockImplementationOnce(async () => [completeUserDataMock]);

      const userToUsersListPresenterSpy = jest
        .spyOn(User, 'toUsersListPresenter')
        .mockImplementationOnce(() => [expectedResult]);

      // when
      const result = await controller.list();

      // then
      expect(userUseCasesListSpy).toBeCalled();
      expect(userToUsersListPresenterSpy).toBeCalledWith([
        completeUserDataMock,
      ]);
      expect(result).toEqual({
        statusCode: 200,
        payload: [expectedResult],
        success: true,
      });
    });
  });

  describe('upsert', () => {
    it('should return the data of upserted user', async () => {
      // given
      const expectedResult = {
        name: completeUserDataMock.name,
        email: completeUserDataMock.email,
        permission: completeUserDataMock.permission,
        storeId: completeUserDataMock.storeId,
        active: completeUserDataMock.active,
        login: completeUserDataMock.login,
        phone: completeUserDataMock.phone,
      };

      const userUseCasesUpsertSpy = jest
        .spyOn(useCases, 'upsert')
        .mockImplementationOnce(async () => completeUserDataMock);

      const userToUpsertUserPresenterSpy = jest
        .spyOn(User, 'toUpsertUserPresenter')
        .mockImplementationOnce(() => expectedResult);

      // when
      const result = await controller.upsert(
        basicUserDataMock as UpsertUserDTO,
      );

      // then
      expect(userUseCasesUpsertSpy).toBeCalledWith(
        basicUserDataMock as UpsertUserDTO,
      );
      expect(userToUpsertUserPresenterSpy).toBeCalledWith({
        ...completeUserDataMock,
      });
      expect(result).toEqual({
        statusCode: 201,
        payload: expectedResult,
        success: true,
      });
    });
  });
});
