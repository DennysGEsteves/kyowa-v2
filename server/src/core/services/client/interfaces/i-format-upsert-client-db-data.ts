import { ClientInterestProducts, ClientOrigins } from '@core/models/client';

export type IFormatUpsertClientDBData = {
  name: string;
  nameFilter?: string;
  cpf?: string;
  rg?: string;
  architectID?: number;
  nasc?: Date;
  occupation?: string;
  email?: string;
  cep?: string;
  address?: string;
  district?: string;
  city?: string;
  region?: string;
  phone1?: string;
  phone2?: string;
  obs?: string;
  active?: boolean;
  interestProducts?: ClientInterestProducts[];
  origins?: ClientOrigins[];
  entry: Date;
};
