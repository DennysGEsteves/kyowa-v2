import { Architect } from '@core/models/architect';
import { IArchitectService, ArchitectService } from '@core/services/architect';
import { formatUpsertArchitectDBData } from '@core/services/architect/transformers/format-upsert-architect-db-data';
import { IArchitectRepository } from '@infra/repositories/architect';
import { Architect as DBArchitect } from '@prisma/client';

import {
  completeArchitectDataMock,
  ArchitectRepositoryMock,
} from '../../../../mocks';

jest.mock(
  '@core/services/architect/transformers/format-upsert-architect-db-data',
);

describe('ArchitectService', () => {
  let architectService: IArchitectService;
  let architectRepository: IArchitectRepository;

  beforeEach(async () => {
    architectRepository = new ArchitectRepositoryMock();
    architectService = new ArchitectService(architectRepository);
  });

  it('should be defined', async () => {
    expect(architectService).toBeDefined();
  });

  describe('list', () => {
    it('should return an architects list', async () => {
      // given
      const architectMock = {
        name: 'some name',
        email: 'some email',
      };

      const dbArchitectsMock = [{ ...architectMock } as DBArchitect];

      const architectRepositoryListSpy = jest
        .spyOn(architectRepository, 'list')
        .mockImplementationOnce(async () => dbArchitectsMock);

      const ArchitectFromArchitectListDBResponseSpy = jest
        .spyOn(Architect, 'fromArchitectListDBResponse')
        .mockImplementationOnce(() => dbArchitectsMock);

      // when
      await architectService.list();

      // then
      expect(architectRepositoryListSpy).toBeCalled();
      expect(ArchitectFromArchitectListDBResponseSpy).toBeCalledWith(
        dbArchitectsMock,
      );
    });
  });

  describe('upsert', () => {
    it('should return a upserted architect', async () => {
      // given
      const architectMock = {
        name: 'some name',
        email: 'some email',
      };

      const dbArchitectMock = { ...architectMock } as DBArchitect;

      const formatUpsertArchitectDBDataSpy = (
        formatUpsertArchitectDBData as jest.Mock
      ).mockImplementation(() => architectMock);

      const architectRepositoryUpsertSpy = jest
        .spyOn(architectRepository, 'upsert')
        .mockImplementationOnce(async () => dbArchitectMock);

      const ArchitectFromArchitectDBResponseSpy = jest
        .spyOn(Architect, 'fromArchitectDBResponse')
        .mockImplementationOnce(() => architectMock);

      // when
      await architectService.upsert(architectMock);

      // then
      expect(architectRepositoryUpsertSpy).toBeCalled();
      expect(ArchitectFromArchitectDBResponseSpy).toBeCalledWith(
        dbArchitectMock,
      );
      expect(formatUpsertArchitectDBDataSpy).toBeCalledWith(architectMock);
    });
  });

  describe('findByEmail', () => {
    it('should return a architect searching by its id', async () => {
      // given
      const dbArchitectMock = { ...completeArchitectDataMock } as DBArchitect;

      const architectRepositoryFindByEmailSpy = jest
        .spyOn(architectRepository, 'findByEmail')
        .mockImplementationOnce(async () => dbArchitectMock);

      // when
      await architectService.findByEmail(dbArchitectMock.email);

      // then
      expect(architectRepositoryFindByEmailSpy).toBeCalledWith(
        dbArchitectMock.email,
      );
    });
  });
});
