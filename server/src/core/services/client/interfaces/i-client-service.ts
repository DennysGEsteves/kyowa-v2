import { Client } from '@core/models/client';

export interface IClientService {
  list: () => Promise<Client[]>;
  upsert(client: Client): Promise<Client>;
  findByName(name: string): Promise<Client>;
}
