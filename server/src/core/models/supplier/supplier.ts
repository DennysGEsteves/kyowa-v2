import { UpsertSupplierDTO } from '@core/controllers/supplier/dtos';
import { SupplierTypes } from '@core/models/supplier';
import { Supplier as DBSupplier } from '@prisma/client';

interface IConstructorParams {
  readonly mid?: string;
  readonly id?: number;
  readonly name: string;
  readonly nameFilter?: string;
  readonly cpnj?: string;
  readonly im?: string;
  readonly ie?: string;
  readonly email?: string;
  readonly cep?: string;
  readonly address?: string;
  readonly district?: string;
  readonly city?: string;
  readonly region?: string;
  readonly phone1?: string;
  readonly phone2?: string;
  readonly obs?: string;
  readonly active?: boolean;
  readonly type?: SupplierTypes;
}

export class Supplier {
  public readonly mid?: string;

  public readonly id?: number;

  public readonly name: string;

  public readonly nameFilter?: string;

  public readonly cpnj?: string;

  public readonly im?: string;

  public readonly ie?: string;

  public readonly email?: string;

  public readonly cep?: string;

  public readonly address?: string;

  public readonly district?: string;

  public readonly city?: string;

  public readonly region?: string;

  public readonly phone1?: string;

  public readonly phone2?: string;

  public readonly obs?: string;

  public readonly active?: boolean;

  public readonly type?: SupplierTypes;

  public constructor({
    id,
    name,
    nameFilter,
    cpnj,
    im,
    ie,
    email,
    cep,
    address,
    district,
    city,
    region,
    phone1,
    phone2,
    obs,
    active = true,
    type = 'product',
  }: IConstructorParams) {
    this.id = id;
    this.name = name;
    this.nameFilter = nameFilter;
    this.cpnj = cpnj;
    this.im = im;
    this.ie = ie;
    this.email = email;
    this.cep = cep;
    this.address = address;
    this.district = district;
    this.city = city;
    this.region = region;
    this.phone1 = phone1;
    this.phone2 = phone2;
    this.obs = obs;
    this.active = active;
    this.type = type;
  }

  public static fromSupplierListDBResponse(data: DBSupplier[]): Supplier[] {
    return data.map(
      (dbSupplier) =>
        new Supplier({
          id: dbSupplier.id,
          name: dbSupplier.name,
          nameFilter: dbSupplier.nameFilter,
          cpnj: dbSupplier.cpnj,
          im: dbSupplier.im,
          ie: dbSupplier.ie,
          email: dbSupplier.email,
          cep: dbSupplier.cep,
          address: dbSupplier.address,
          district: dbSupplier.district,
          city: dbSupplier.city,
          region: dbSupplier.region,
          phone1: dbSupplier.phone1,
          phone2: dbSupplier.phone2,
          obs: dbSupplier.obs,
          active: dbSupplier.active,
          type: dbSupplier.type,
        }),
    );
  }

  public static fromSupplierDBResponse(dbSupplier: DBSupplier): Supplier {
    return new Supplier({
      id: dbSupplier.id,
      name: dbSupplier.name,
      nameFilter: dbSupplier.nameFilter,
      cpnj: dbSupplier.cpnj,
      im: dbSupplier.im,
      ie: dbSupplier.ie,
      email: dbSupplier.email,
      cep: dbSupplier.cep,
      address: dbSupplier.address,
      district: dbSupplier.district,
      city: dbSupplier.city,
      region: dbSupplier.region,
      phone1: dbSupplier.phone1,
      phone2: dbSupplier.phone2,
      obs: dbSupplier.obs,
      active: dbSupplier.active,
      type: dbSupplier.type,
    });
  }

  public static fromUpsertSupplierDTO(dto: UpsertSupplierDTO): Supplier {
    return new Supplier({
      name: dto.name,
      nameFilter: dto.nameFilter,
      cpnj: dto.cpnj,
      im: dto.im,
      ie: dto.ie,
      email: dto.email,
      cep: dto.cep,
      address: dto.address,
      district: dto.district,
      city: dto.city,
      region: dto.region,
      phone1: dto.phone1,
      phone2: dto.phone2,
      obs: dto.obs,
    });
  }

  public static toSuppliersListPresenter = (
    suppliers: Supplier[],
  ): Supplier[] => {
    return suppliers.map((supplier) => {
      return {
        name: supplier.name,
        nameFilter: supplier.nameFilter,
        cpnj: supplier.cpnj,
        im: supplier.im,
        ie: supplier.ie,
        email: supplier.email,
        cep: supplier.cep,
        address: supplier.address,
        district: supplier.district,
        city: supplier.city,
        region: supplier.region,
        phone1: supplier.phone1,
        phone2: supplier.phone2,
        obs: supplier.obs,
        active: supplier.active,
        type: supplier.type,
      };
    });
  };

  public static toUpsertSupplierPresenter = (supplier: Supplier): Supplier => {
    return {
      id: supplier.id,
      name: supplier.name,
      nameFilter: supplier.nameFilter,
      cpnj: supplier.cpnj,
      im: supplier.im,
      ie: supplier.ie,
      email: supplier.email,
      cep: supplier.cep,
      address: supplier.address,
      district: supplier.district,
      city: supplier.city,
      region: supplier.region,
      phone1: supplier.phone1,
      phone2: supplier.phone2,
      obs: supplier.obs,
      active: supplier.active,
      type: supplier.type,
    };
  };

  public static tologgerPresenter = (supplier: Supplier): Supplier => {
    return {
      name: supplier.name,
      nameFilter: supplier.nameFilter,
      cpnj: supplier.cpnj,
      im: supplier.im,
      ie: supplier.ie,
      email: supplier.email,
      cep: supplier.cep,
      address: supplier.address,
      district: supplier.district,
      city: supplier.city,
      region: supplier.region,
      phone1: supplier.phone1,
      phone2: supplier.phone2,
      obs: supplier.obs,
      active: supplier.active,
      type: supplier.type,
    };
  };
}
