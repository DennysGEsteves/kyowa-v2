import { Client } from '@core/models/client';

import { IFormatUpsertClientDBData } from '../interfaces';

export const formatUpsertClientDBData = (
  client: Client,
): IFormatUpsertClientDBData => {
  return {
    name: client.name,
    nameFilter: client.nameFilter,
    cpf: client.cpf,
    rg: client.rg,
    architectID: client.architectID,
    nasc: client.nasc,
    occupation: client.occupation,
    email: client.email,
    cep: client.cep,
    address: client.address,
    district: client.district,
    city: client.city,
    region: client.region,
    phone1: client.phone1,
    phone2: client.phone2,
    obs: client.obs,
    active: client.active,
    interestProducts: client.interestProducts,
    origins: client.origins,
    entry: client.entry,
  };
};
