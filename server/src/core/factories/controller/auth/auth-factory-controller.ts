import { AuthController } from '@core/controllers/auth';
import { UserService } from '@core/services/user';
import { AuthUseCases } from '@core/use-cases/auth';
import { UserRepository } from '@infra/repositories/user';

export const authFactoryController = (): AuthController => {
  const repository = new UserRepository();
  const service = new UserService(repository);
  const useCases = new AuthUseCases(service);
  const controller = new AuthController(useCases);
  return controller;
};
