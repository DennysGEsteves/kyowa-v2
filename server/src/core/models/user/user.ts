import { UpsertUserDTO } from '@core/controllers/user/dtos';
import { UserPermissions } from '@core/models/user';
import { UtilsLogin } from '@core/utils';
import { User as DBUser } from '@prisma/client';

interface IConstructorParams {
  readonly mid?: string;
  readonly id?: number;
  readonly name: string;
  readonly email: string;
  readonly permission: UserPermissions;
  readonly storeId: number;
  readonly login?: string;
  readonly active?: boolean;
  readonly phone?: string;
  readonly pass?: string;
}

export class User {
  public readonly mid?: string;

  public readonly id?: number;

  public readonly name?: string;

  public readonly email: string;

  public readonly permission?: UserPermissions;

  public readonly storeId?: number;

  public readonly login?: string;

  public readonly active?: boolean;

  public readonly phone?: string;

  public readonly pass?: string;

  public constructor({
    name,
    email,
    permission,
    storeId,
    login = null,
    phone = null,
    active = false,
    pass = null,
    id,
  }: IConstructorParams) {
    this.name = name;
    this.email = email;
    this.permission = permission;
    this.storeId = storeId;
    this.phone = phone;
    this.active = active;
    this.login = login;
    this.pass = pass;
    this.id = id;
  }

  public static fromUserListDBResponse(data: DBUser[]): User[] {
    return data.map(
      (dbUser) =>
        new User({
          id: dbUser.id,
          login: dbUser.login,
          name: dbUser.name,
          email: dbUser.email,
          permission: dbUser.permission,
          storeId: dbUser.storeId,
          active: dbUser.active,
          phone: dbUser.phone,
        }),
    );
  }

  public static fromUserDBResponse(dbUser: DBUser): User {
    return new User({
      id: dbUser.id,
      login: dbUser.login,
      name: dbUser.name,
      email: dbUser.email,
      permission: dbUser.permission,
      storeId: dbUser.storeId,
      active: dbUser.active,
      phone: dbUser.phone,
    });
  }

  public static fromUpsertUserDTO(dto: UpsertUserDTO): User {
    return new User({
      email: dto.email,
      name: dto.name,
      permission: dto.permission,
      storeId: dto.storeId,
      phone: dto.phone,
      id: dto.id,
      login:
        !dto.login && !dto.id
          ? UtilsLogin.genLoginFromEmail(dto.email)
          : dto.login,
      pass: !dto.pass && !dto.id ? process.env.DEFAULT_USER_PASS : dto.pass,
    });
  }

  public static toUsersListPresenter = (users: User[]): User[] => {
    return users.map((user) => {
      return {
        id: user.id,
        name: user.name,
        email: user.email,
        permission: user.permission,
        storeId: user.storeId,
        active: user.active,
        login: user.login,
        phone: user.phone,
      };
    });
  };

  public static toUpsertUserPresenter = (user: User): User => {
    return {
      id: user.id,
      name: user.name,
      email: user.email,
      permission: user.permission,
      storeId: user.storeId,
      active: user.active,
      login: user.login,
      phone: user.phone,
    };
  };

  public static toLoggerPresenter = (user: User): User => {
    return {
      id: user?.id,
      name: user?.name,
      email: user?.email,
      permission: user?.permission,
      storeId: user?.storeId,
      active: user?.active,
      login: user?.login,
      phone: user?.phone,
    };
  };
}
