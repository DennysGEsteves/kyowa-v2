import { UpsertArchitectDTO } from '@core/controllers/architect/dtos';
import { Architect } from '@core/models/architect';
import { IArchitectUseCases } from '@core/use-cases/architect/interfaces';
import { IResponseBody, ResponseTypes } from '@infra/http/response-controller';

export class ArchitectController {
  constructor(private architectUseCases: IArchitectUseCases) {}

  async list(): Promise<IResponseBody> {
    const architect = await this.architectUseCases.list();
    return ResponseTypes.ok(Architect.toArchitectsListPresenter(architect));
  }

  async upsert(dto: UpsertArchitectDTO): Promise<IResponseBody> {
    const architect = await this.architectUseCases.upsert(dto);
    return ResponseTypes.created(
      Architect.toUpsertArchitectPresenter(architect),
    );
  }
}
