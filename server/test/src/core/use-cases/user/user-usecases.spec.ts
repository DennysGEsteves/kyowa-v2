import { UpsertUserDTO } from '@core/controllers/user/dtos';
import { User } from '@core/models/user';
import { IUserService } from '@core/services/user';
import { IUserUseCases, UserUseCases } from '@core/use-cases/user';

import { basicUserDataMock, UserServiceMock } from '../../../../mocks';

describe('UserUseCases', () => {
  let userUseCases: IUserUseCases;
  let userService: IUserService;

  beforeEach(async () => {
    userService = new UserServiceMock();
    userUseCases = new UserUseCases(userService);
  });

  it('should be defined', async () => {
    expect(userUseCases).toBeDefined();
  });

  describe('list', () => {
    it('should return a Users list', async () => {
      // given
      const listUsersSpy = jest
        .spyOn(userService, 'list')
        .mockImplementationOnce(async () => []);

      // when
      await userUseCases.list();

      // then
      expect(listUsersSpy).toBeCalled();
    });
  });

  describe('upsert', () => {
    it('should return the data of upserted user', async () => {
      // given
      const fromUpsertUserDTOMock = {
        ...basicUserDataMock,
        login: 'someemail',
        pass: process.env.DEFAULT_USER_PASS,
      } as User;

      const upsertedUsersSpy = jest
        .spyOn(userService, 'upsert')
        .mockImplementationOnce(async () => undefined);

      const fromUpsertUserDTOSpy = jest
        .spyOn(User, 'fromUpsertUserDTO')
        .mockImplementationOnce(() => fromUpsertUserDTOMock);

      // when
      await userUseCases.upsert(basicUserDataMock as UpsertUserDTO);

      // then
      expect(fromUpsertUserDTOSpy).toBeCalledWith(basicUserDataMock);
      expect(upsertedUsersSpy).toBeCalledWith(fromUpsertUserDTOMock);
    });
  });
});
