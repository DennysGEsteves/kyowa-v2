import { Request, Response } from 'express';

import * as AuthService from '@services/AuthService';

export const login = async (req: Request, res: Response): Promise<void> => {
  const { email, password } = req.body;

  const token = await AuthService.loginValidation(email, password);
  if (token) {
    res.json({ auth: true, token });
  } else {
    res.sendStatus(401);
  }
};
