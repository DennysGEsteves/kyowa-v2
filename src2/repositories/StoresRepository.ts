import { IStoreCreate } from 'src/types/Stores';

import { Store } from '@prisma/client';

import prisma from './prismaClient';

const { store: db } = prisma;

export const create = (data: IStoreCreate): Promise<Store> => {
  const { managerId, ...rest } = data;

  return db.create({
    data: {
      ...rest,
      manager: {
        connect: {
          id: managerId,
        },
      },
    },
  });
};

export const update = (id: number, data: Store): Promise<Store> =>
  db.update({
    where: { id },
    data,
  });

export const remove = (id: number): Promise<Store> =>
  db.delete({
    where: { id },
  });

export const list = (): Promise<Store[]> => db.findMany();

export const getByName = (name: string): Promise<Store> =>
  db.findFirst({
    where: { name },
  });

export const getByEmail = (email: string): Promise<Store> =>
  db.findFirst({
    where: { email },
  });
