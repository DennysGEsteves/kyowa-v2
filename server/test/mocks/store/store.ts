export const basicStoreDataMock = {
  name: 'some name',
};

export const completeStoreDataMock = {
  name: 'some name',
  address: 'some address',
  cep: 'some cep',
  city: 'some city',
  district: 'some district',
  email: 'some@email.com',
  id: 999999,
  managerId: 99999,
  region: 'some region',
  obs: 'some obs',
  phone1: 'some phone 1',
  phone2: 'some phone 2',
};
