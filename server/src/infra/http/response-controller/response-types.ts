import { IResponseBody } from './types';

export class ResponseTypes {
  public static ok<T>(payload?: T): IResponseBody {
    return {
      statusCode: 200,
      payload,
      success: true,
    };
  }

  public static created<T>(payload?: T): IResponseBody {
    return {
      statusCode: 201,
      payload,
      success: true,
    };
  }

  public static badRequest(error: Error): IResponseBody {
    return {
      statusCode: 400,
      payload: { error: error.message },
      success: false,
    };
  }

  public static unprocessableEntity(error: Error): IResponseBody {
    return {
      statusCode: 422,
      payload: { error: error.message },
      success: false,
    };
  }

  public static unauthorized(error: Error): IResponseBody {
    return {
      statusCode: 401,
      payload: { error: error.message },
      success: false,
    };
  }

  public static forbidden(error: Error): IResponseBody {
    return {
      statusCode: 403,
      payload: { error: error.message },
      success: false,
    };
  }

  public static notFound(error: Error): IResponseBody {
    return {
      statusCode: 404,
      payload: { error: error.message },
      success: false,
    };
  }

  public static conflict(error: Error): IResponseBody {
    return {
      statusCode: 409,
      payload: { error: error.message },
      success: false,
    };
  }

  public static tooMany(error: Error): IResponseBody {
    return {
      statusCode: 429,
      payload: { error: error.message },
      success: false,
    };
  }

  public static fail(error: Error): IResponseBody {
    console.log(error);

    return {
      statusCode: 500,
      payload: { error: error.message },
      success: false,
    };
  }
}
