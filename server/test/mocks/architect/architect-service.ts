import { IArchitectService } from '@core/services/architect';

export const ArchitectServiceMock = jest.fn().mockImplementation(() => {
  return {
    list: jest.fn(),
    upsert: jest.fn(),
    findByEmail: jest.fn(),
  } as IArchitectService;
});
