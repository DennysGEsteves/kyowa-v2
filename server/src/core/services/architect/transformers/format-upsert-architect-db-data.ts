import { Architect } from '@core/models/architect';

import { IFormatUpsertArchitectDBData } from '../interfaces';

export const formatUpsertArchitectDBData = (
  architect: Architect,
): IFormatUpsertArchitectDBData => {
  return {
    email: architect.email,
    name: architect.name,
    cpf: architect.cpf,
    nasc: architect.nasc,
    active: architect.active,
    cep: architect.cep,
    address: architect.address,
    district: architect.district,
    city: architect.city,
    region: architect.region,
    phone1: architect.phone1,
    phone2: architect.phone2,
    obs: architect.obs,
    sellerId: architect.sellerId,
  };
};
