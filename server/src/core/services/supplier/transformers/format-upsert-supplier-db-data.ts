import { Supplier } from '@core/models/supplier';

import { IFormatUpsertSupplierDBData } from '../interfaces';

export const formatUpsertSupplierDBData = (
  supplier: Supplier,
): IFormatUpsertSupplierDBData => {
  return {
    id: supplier.id,
    name: supplier.name,
    nameFilter: supplier.nameFilter,
    cpnj: supplier.cpnj,
    im: supplier.im,
    ie: supplier.ie,
    email: supplier.email,
    cep: supplier.cep,
    address: supplier.address,
    district: supplier.district,
    city: supplier.city,
    region: supplier.region,
    phone1: supplier.phone1,
    phone2: supplier.phone2,
    obs: supplier.obs,
    active: supplier.active,
    type: supplier.type,
  };
};
