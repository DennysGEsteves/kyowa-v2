import * as jwt from 'jsonwebtoken';

import * as UserRepository from '@repositories/UserRepository';

export const genToken = (email: string): string => {
  const token = jwt.sign({ email }, process.env.JWT_SECRET, {
    expiresIn: 60 * 1000 * 3, // 3 horas
  });
  return token;
};

export const loginValidation = async (
  email: string,
  password: string,
): Promise<boolean | string> => {
  const user = await UserRepository.getByEmail(email);

  if (user) {
    if (user.pass === password) {
      return genToken(email);
    }
    return false;
  }
  return false;
};
