import { UpsertStoreDTO } from '@core/controllers/store/dtos';
import { Store } from '@core/models/store';
import { Store as DBStore } from '@prisma/client';

import { basicStoreDataMock, completeStoreDataMock } from '../../../../mocks';

describe('StoreModel', () => {
  it('should return an instance of Store with all parameters', () => {
    // given

    // when
    const store = new Store(completeStoreDataMock);

    // then
    expect(store).toBeInstanceOf(Store);
  });

  it('should create an instance with default params only', () => {
    // given
    process.env.DEFAULT_USER_PASS = 'some default pass';

    // when
    const store = new Store(basicStoreDataMock);

    // then
    expect(store).toBeInstanceOf(Store);
  });

  describe('fromStoreListDBResponse', () => {
    it('should create an instance of Store from database list', () => {
      // given
      const dbStoresListMock = [completeStoreDataMock] as DBStore[];

      // when
      const store = Store.fromStoreListDBResponse(dbStoresListMock);

      // then
      expect(store[0]).toBeInstanceOf(Store);
    });
  });

  describe('fromStoreDBResponse', () => {
    it('should create an instance of Store from database item', () => {
      // given
      const dbStoreMock = completeStoreDataMock as DBStore;

      // when
      const store = Store.fromStoreDBResponse(dbStoreMock);

      // then
      expect(store).toBeInstanceOf(Store);
    });
  });

  describe('fromUpsertStoreDTO', () => {
    it('should create an instance of Store from upsert dto', () => {
      // given
      const dtoStoreMock = completeStoreDataMock as UpsertStoreDTO;

      // when
      const store = Store.fromUpsertStoreDTO(dtoStoreMock);

      // then
      expect(store).toBeInstanceOf(Store);
    });
  });

  describe('toStoresListPresenter', () => {
    it('should create a presenter store', () => {
      // given
      const storeMock = [completeStoreDataMock] as Store[];

      // when
      const stores = Store.toStoresListPresenter(storeMock);

      // then
      expect(stores[0]).toEqual({
        id: stores[0].id,
        name: stores[0].name,
        email: stores[0].email,
        address: stores[0].address,
        cep: stores[0].cep,
        city: stores[0].city,
        district: stores[0].district,
        managerId: stores[0].managerId,
        obs: stores[0].obs,
        phone1: stores[0].phone1,
        phone2: stores[0].phone2,
        region: stores[0].region,
      });
    });
  });

  describe('toUpsertStorePresenter', () => {
    it('should create a presenter store', () => {
      // given
      const storeMock = completeStoreDataMock as Store;

      // when
      const store = Store.toUpsertStorePresenter(storeMock);

      // then
      expect(store).toEqual({
        id: store.id,
        name: store.name,
        email: store.email,
        address: store.address,
        cep: store.cep,
        city: store.city,
        district: store.district,
        managerId: store.managerId,
        obs: store.obs,
        phone1: store.phone1,
        phone2: store.phone2,
        region: store.region,
      });
    });
  });
});
