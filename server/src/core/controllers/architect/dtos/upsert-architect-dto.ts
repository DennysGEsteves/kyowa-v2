import { Type } from 'class-transformer';
import {
  IsBoolean,
  IsDate,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

export class UpsertArchitectDTO {
  @IsOptional()
  @IsNumber()
  readonly id: number;

  @IsNotEmpty()
  @IsString()
  readonly name: string;

  @IsOptional()
  @IsString()
  readonly name_filter: string;

  @IsOptional()
  @IsString()
  readonly cpf: string;

  @IsOptional()
  @IsDate()
  @Type(() => Date)
  readonly nasc: Date;

  @IsOptional()
  @IsString()
  readonly email: string;

  @IsOptional()
  @IsString()
  readonly cep: string;

  @IsOptional()
  @IsString()
  readonly address: string;

  @IsOptional()
  @IsString()
  readonly district: string;

  @IsOptional()
  @IsString()
  readonly city: string;

  @IsOptional()
  @IsString()
  readonly region: string;

  @IsOptional()
  @IsString()
  readonly phone1: string;

  @IsOptional()
  @IsString()
  readonly phone2: string;

  @IsOptional()
  @IsString()
  readonly obs: string;

  @IsOptional()
  @IsBoolean()
  readonly active: boolean;

  @IsOptional()
  @IsNumber()
  readonly sellerId: number;
}
