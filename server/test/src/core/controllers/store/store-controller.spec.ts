import { StoreController } from '@core/controllers/store';
import { UpsertStoreDTO } from '@core/controllers/store/dtos';
import { Store } from '@core/models/store';
import { IStoreUseCases } from '@core/use-cases/store';

import {
  basicStoreDataMock,
  completeStoreDataMock,
  StoreUseCasesMock,
} from '../../../../mocks/store';

describe('StoreController', () => {
  let controller: StoreController;
  let useCases: IStoreUseCases;

  beforeEach(async () => {
    useCases = new StoreUseCasesMock();
    controller = new StoreController(useCases);
  });

  it('should be defined', async () => {
    expect(controller).toBeDefined();
  });

  describe('list', () => {
    it('should return a Stores list', async () => {
      // given
      const expectedResult = {
        id: completeStoreDataMock.id,
        name: completeStoreDataMock.name,
        email: completeStoreDataMock.email,
        address: completeStoreDataMock.address,
        cep: completeStoreDataMock.cep,
        city: completeStoreDataMock.city,
        district: completeStoreDataMock.district,
        managerId: completeStoreDataMock.managerId,
        obs: completeStoreDataMock.obs,
        phone1: completeStoreDataMock.phone1,
        phone2: completeStoreDataMock.phone2,
        region: completeStoreDataMock.region,
      };

      const useCasesListSpy = jest
        .spyOn(useCases, 'list')
        .mockImplementationOnce(async () => [completeStoreDataMock]);

      const storeToStoresListPresenter = jest
        .spyOn(Store, 'toStoresListPresenter')
        .mockImplementationOnce(() => [expectedResult]);

      // when
      const result = await controller.list();

      // then
      expect(useCasesListSpy).toBeCalled();
      expect(storeToStoresListPresenter).toBeCalledWith([
        completeStoreDataMock,
      ]);
      expect(result).toEqual({
        statusCode: 200,
        payload: [expectedResult],
        success: true,
      });
    });
  });

  describe('upsert', () => {
    it('should return the data of upserted user', async () => {
      // given
      const expectedResult = {
        id: completeStoreDataMock.id,
        name: completeStoreDataMock.name,
        email: completeStoreDataMock.email,
        address: completeStoreDataMock.address,
        cep: completeStoreDataMock.cep,
        city: completeStoreDataMock.city,
        district: completeStoreDataMock.district,
        managerId: completeStoreDataMock.managerId,
        obs: completeStoreDataMock.obs,
        phone1: completeStoreDataMock.phone1,
        phone2: completeStoreDataMock.phone2,
        region: completeStoreDataMock.region,
      };

      const userUseCasesUpsertSpy = jest
        .spyOn(useCases, 'upsert')
        .mockImplementationOnce(async () => completeStoreDataMock);

      const storeToUpsertStorePresenterSpy = jest
        .spyOn(Store, 'toUpsertStorePresenter')
        .mockImplementationOnce(() => expectedResult);

      // when
      const result = await controller.upsert(
        basicStoreDataMock as UpsertStoreDTO,
      );

      // then
      expect(userUseCasesUpsertSpy).toBeCalledWith(
        basicStoreDataMock as UpsertStoreDTO,
      );
      expect(storeToUpsertStorePresenterSpy).toBeCalledWith(
        completeStoreDataMock,
      );
      expect(result).toEqual({
        statusCode: 201,
        payload: expectedResult,
        success: true,
      });
    });
  });
});
