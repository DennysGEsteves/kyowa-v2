export const basicArchitectDataMock = {
  name: 'some name',
};

export const completeArchitectDataMock = {
  name: 'some name',
  address: 'some address',
  cep: 'some cep',
  city: 'some city',
  district: 'some district',
  email: 'some@email.com',
  id: 999999,
  managerId: 99999,
  region: 'some region',
  obs: 'some obs',
  phone1: 'some phone 1',
  phone2: 'some phone 2',
  active: true,
  cpf: 'some cpf',
  name_filter: 'some_name',
  nasc: new Date('2022-05-05T00:00:00.000Z'),
  sellerId: 999999,
};
