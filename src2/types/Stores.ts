import { Store } from '@prisma/client';

export type IStoreCreate = Omit<Store, 'id'>;
