export type ArchitectPermissions =
  | 'admin'
  | 'manager'
  | 'sales'
  | 'operational'
  | 'finance';
