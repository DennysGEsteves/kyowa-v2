import { Router } from 'express';

import * as ArchitectController from '@controllers/ArchitectController';

const router = Router();

router.post('/list', ArchitectController.list);
router.post('/', ArchitectController.create);
router.get('/image/:Id', ArchitectController.getImage);
router.put('/:architectId', ArchitectController.update);
router.delete('/:architectId', ArchitectController.remove);

export default router;
