import bcrypt from 'bcrypt';
import { Application } from 'express';
import * as jwt from 'jsonwebtoken';
import request from 'supertest';

import { AuthenticateUserDTO } from '@core/controllers/auth/dtos';
import { UserService } from '@core/services/user';
import { User as DBUser } from '@prisma/client';

import App from '../../../../../../src/app';

describe('AuthApi Endpoints', () => {
  let app: Application;

  beforeAll(async () => {
    app = App.app;
  });

  describe('POST /api/auth', () => {
    it('should map route properly', async () => {
      const dbUserMock = {
        id: 564654,
        email: 'some@email.com',
      } as DBUser;

      jest.spyOn(bcrypt, 'compareSync').mockImplementationOnce(() => true);
      jest.spyOn(jwt, 'sign').mockImplementationOnce(() => 'sometoken');

      jest
        .spyOn(UserService.prototype, 'findByEmail')
        .mockImplementationOnce(async () => dbUserMock);

      const res = await request(app)
        .post('/api/auth')
        .send({
          email: 'some@email.com',
          pass: 'some pass',
        } as AuthenticateUserDTO);

      expect(res.statusCode).toEqual(200);
      expect(res.body).toHaveProperty('payload');
      expect(res.body.payload).toHaveProperty('token');
      expect(res.body).toHaveProperty('success');
    });

    it('should return invalid credentials error', async () => {
      jest
        .spyOn(UserService.prototype, 'findByEmail')
        .mockImplementationOnce(async () => undefined);

      const res = await request(app)
        .post('/api/auth')
        .send({
          email: 'some@email.com',
          pass: 'some pass',
        } as AuthenticateUserDTO);

      expect(res.statusCode).toEqual(401);
      expect(res.body).toEqual({
        success: false,
        payload: { error: 'Email/Senha incorretos' },
      });
    });
  });
});
