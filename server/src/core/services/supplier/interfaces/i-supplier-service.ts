import { Supplier } from '@core/models/supplier';

export interface ISupplierService {
  list: () => Promise<Supplier[]>;
  upsert(supplier: Supplier): Promise<Supplier>;
}
