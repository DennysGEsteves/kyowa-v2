import { UpsertClientDTO } from '@core/controllers/client/dtos';
import { Client } from '@core/models/client';

export interface IClientUseCases {
  list: () => Promise<Client[]>;
  upsert(dto: UpsertClientDTO): Promise<Client>;
}
