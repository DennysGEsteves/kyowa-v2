import { User as DBUser } from '@prisma/client';

import { IFormatUpsertUserDBData } from '../../../../core/services/user/interfaces/i-format-upsert-user-db-data';

export interface IUserRepository {
  list: () => Promise<DBUser[]>;
  upsert: (user: IFormatUpsertUserDBData) => Promise<DBUser>;
  findByEmail(email: string): Promise<DBUser>;
  remove(id: number): Promise<DBUser>;
}
