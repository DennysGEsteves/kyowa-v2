import { Store } from '@core/models/store';

export interface IStoreService {
  list: () => Promise<Store[]>;
  upsert(store: Store): Promise<Store>;
  findById(id: number): Promise<Store>;
}
