import { Architect as DBArchitect } from '@prisma/client';

import { IFormatUpsertArchitectDBData } from '../../../../core/services/architect/interfaces/i-format-upsert-architect-db-data';

export interface IArchitectRepository {
  list: () => Promise<DBArchitect[]>;
  upsert: (
    architect: IFormatUpsertArchitectDBData,
    id: number,
  ) => Promise<DBArchitect>;
  findByEmail(email: string): Promise<DBArchitect>;
}
