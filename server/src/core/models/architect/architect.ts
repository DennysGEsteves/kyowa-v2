// import { ArchitectPermissions } from '@core/models/architect';
// import { UtilsLogin } from '@core/utils';
import { UpsertArchitectDTO } from '@core/controllers/architect/dtos';
import { Architect as DBArchitect } from '@prisma/client';

interface IConstructorParams {
  readonly mid?: string;
  readonly id?: number;
  readonly name: string;
  readonly name_filter?: string;
  readonly cpf?: string;
  readonly nasc?: Date;
  readonly email?: string;
  readonly cep?: string;
  readonly address?: string;
  readonly district?: string;
  readonly city?: string;
  readonly region?: string;
  readonly phone1?: string;
  readonly phone2?: string;
  readonly obs?: string;
  readonly active?: boolean;
  readonly sellerId?: number;
}

export class Architect {
  public readonly mid?: string;

  public readonly id?: number;

  public readonly name: string;

  public readonly name_filter?: string;

  public readonly cpf?: string;

  public readonly nasc?: Date;

  public readonly email?: string;

  public readonly cep?: string;

  public readonly address?: string;

  public readonly district?: string;

  public readonly city?: string;

  public readonly region?: string;

  public readonly phone1?: string;

  public readonly phone2?: string;

  public readonly obs?: string;

  public readonly active?: boolean;

  public readonly sellerId?: number;

  public constructor({
    id,
    email,
    name,
    cpf,
    nasc,
    active,
    cep,
    address,
    district,
    city,
    region,
    phone1,
    phone2,
    obs,
    sellerId,
  }: IConstructorParams) {
    this.id = id;
    this.email = email;
    this.name = name;
    this.cpf = cpf;
    this.nasc = nasc;
    this.active = active;
    this.cep = cep;
    this.address = address;
    this.district = district;
    this.city = city;
    this.region = region;
    this.phone1 = phone1;
    this.phone2 = phone2;
    this.obs = obs;
    this.sellerId = sellerId;
  }

  public static fromArchitectListDBResponse(data: DBArchitect[]): Architect[] {
    return data.map(
      (dbArchitect) =>
        new Architect({
          id: dbArchitect.id,
          email: dbArchitect.email,
          name: dbArchitect.name,
          cpf: dbArchitect.cpf,
          nasc: dbArchitect.nasc,
          active: dbArchitect.active,
          cep: dbArchitect.cep,
          address: dbArchitect.address,
          district: dbArchitect.district,
          city: dbArchitect.city,
          region: dbArchitect.region,
          phone1: dbArchitect.phone1,
          phone2: dbArchitect.phone2,
          obs: dbArchitect.obs,
          sellerId: dbArchitect.sellerId,
        }),
    );
  }

  public static fromArchitectDBResponse(dbArchitect: DBArchitect): Architect {
    return new Architect({
      id: dbArchitect.id,
      email: dbArchitect.email,
      name: dbArchitect.name,
      cpf: dbArchitect.cpf,
      nasc: dbArchitect.nasc,
      active: dbArchitect.active,
      cep: dbArchitect.cep,
      address: dbArchitect.address,
      district: dbArchitect.district,
      city: dbArchitect.city,
      region: dbArchitect.region,
      phone1: dbArchitect.phone1,
      phone2: dbArchitect.phone2,
      obs: dbArchitect.obs,
      sellerId: dbArchitect.sellerId,
    });
  }

  public static fromUpsertArchitectDTO(dto: UpsertArchitectDTO): Architect {
    return new Architect({
      id: dto.id,
      email: dto.email,
      name: dto.name,
      cpf: dto.cpf,
      nasc: dto.nasc,
      active: dto.active,
      cep: dto.cep,
      address: dto.address,
      district: dto.district,
      city: dto.city,
      region: dto.region,
      phone1: dto.phone1,
      phone2: dto.phone2,
      obs: dto.obs,
      sellerId: dto.sellerId,
    });
  }

  public static toArchitectsListPresenter = (
    architects: Architect[],
  ): Architect[] => {
    return architects.map((architect) => {
      return {
        id: architect.id,
        email: architect.email,
        name: architect.name,
        cpf: architect.cpf,
        nasc: architect.nasc,
        active: architect.active,
        cep: architect.cep,
        address: architect.address,
        district: architect.district,
        city: architect.city,
        region: architect.region,
        phone1: architect.phone1,
        phone2: architect.phone2,
        obs: architect.obs,
        sellerId: architect.sellerId,
      };
    });
  };

  public static toUpsertArchitectPresenter = (
    architect: Architect,
  ): Architect => {
    return {
      id: architect.id,
      email: architect.email,
      name: architect.name,
      cpf: architect.cpf,
      nasc: architect.nasc,
      active: architect.active,
      cep: architect.cep,
      address: architect.address,
      district: architect.district,
      city: architect.city,
      region: architect.region,
      phone1: architect.phone1,
      phone2: architect.phone2,
      obs: architect.obs,
      sellerId: architect.sellerId,
    };
  };
}
