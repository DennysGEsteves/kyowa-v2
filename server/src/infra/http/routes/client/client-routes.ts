import { Request, Response, Router } from 'express';

import { clientFactoryController } from '@core/factories/controller/client';
import { validateDto } from '@infra/http/middlewares/dto-validator';
import { ResponseController } from '@infra/http/response-controller';

import { UpsertClientDTO } from '../../../../core/controllers/client/dtos';

class ClientRoutes {
  router = Router();

  clientController = clientFactoryController();

  constructor() {
    this.list();
    this.upsert();
  }

  list() {
    this.router.get('/', async (req: Request, res: Response) => {
      return ResponseController.toJson(req, res, this.clientController.list());
    });
  }

  upsert() {
    this.router.post(
      '/',
      validateDto(UpsertClientDTO),
      async (req: Request, res: Response) => {
        return ResponseController.toJson(
          req,
          res,
          this.clientController.upsert({ ...req.body }),
        );
      },
    );
  }
}

export default new ClientRoutes().router;
