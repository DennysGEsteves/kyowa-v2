import { IFormatUpsertStoreDBData } from '@core/services/store';
import { Store as DBStore } from '@prisma/client';

export interface IStoreRepository {
  list: () => Promise<DBStore[]>;
  upsert: (store: IFormatUpsertStoreDBData, id: number) => Promise<DBStore>;
  findById(id: number): Promise<DBStore>;
}
