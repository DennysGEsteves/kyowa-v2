import { AuthController } from '@core/controllers/auth';
import { AuthenticateUserDTO } from '@core/controllers/auth/dtos';
import { IAuthUseCases } from '@core/use-cases/auth';

import { AuthUseCasesMock } from '../../../../mocks/auth';

describe('AuthController', () => {
  let controller: AuthController;
  let useCases: IAuthUseCases;

  beforeEach(async () => {
    useCases = new AuthUseCasesMock();
    controller = new AuthController(useCases);
  });

  it('should be defined', async () => {
    expect(controller).toBeDefined();
  });

  describe('authenticate', () => {
    it('should return a authentication token', async () => {
      // given
      const signinDtoMock = {
        email: 'some email',
        pass: 'some pass',
      } as AuthenticateUserDTO;

      const tokenMock = 'sometokenmock';

      const useCasesAuthenticateSpy = jest
        .spyOn(useCases, 'authenticate')
        .mockImplementationOnce(async () => tokenMock);

      // when
      const result = await controller.authenticate(signinDtoMock);

      // then
      expect(useCasesAuthenticateSpy).toBeCalledWith(signinDtoMock);
      expect(result).toEqual({
        statusCode: 200,
        payload: { token: tokenMock },
        success: true,
      });
    });
  });
});
