export enum ClientOrigins {
  'friends' = 'friends',
  'architect' = 'architect',
  'internet' = 'internet',
  'relatives' = 'relatives',
  'radio' = 'radio',
  'socialNetwork' = 'socialNetwork',
  'tv' = 'tv',
}
