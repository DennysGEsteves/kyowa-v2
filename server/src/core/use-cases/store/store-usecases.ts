import { UpsertStoreDTO } from '@core/controllers/store/dtos';
import { Store } from '@core/models/store';
import { IStoreService } from '@core/services/store';

import { IStoreUseCases } from './interfaces';

export class StoreUseCases implements IStoreUseCases {
  constructor(private storeService: IStoreService) {}

  list(): Promise<Store[]> {
    return this.storeService.list();
  }

  upsert(dto: UpsertStoreDTO): Promise<Store> {
    const store = Store.fromUpsertStoreDTO(dto);
    return this.storeService.upsert(store);
  }
}
