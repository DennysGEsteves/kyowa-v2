import { ArchitectController } from '@core/controllers/architect';
import { Architect } from '@core/models/architect';
import { IArchitectUseCases } from '@core/use-cases/architect';
import { UpsertArchitectDTO } from '@infra/http/routes/architect';

import {
  basicArchitectDataMock,
  completeArchitectDataMock,
  ArchitectUseCasesMock,
} from '../../../../mocks/architect';

describe('ArchitectController', () => {
  let controller: ArchitectController;
  let useCases: IArchitectUseCases;

  beforeEach(async () => {
    useCases = new ArchitectUseCasesMock();
    controller = new ArchitectController(useCases);
  });

  it('should be defined', async () => {
    expect(controller).toBeDefined();
  });

  describe('list', () => {
    it('should return a Architects list', async () => {
      // given
      const expectedResult = {
        id: completeArchitectDataMock.id,
        email: completeArchitectDataMock.email,
        name: completeArchitectDataMock.name,
        cpf: completeArchitectDataMock.cpf,
        nasc: completeArchitectDataMock.nasc,
        active: completeArchitectDataMock.active,
        cep: completeArchitectDataMock.cep,
        address: completeArchitectDataMock.address,
        district: completeArchitectDataMock.district,
        city: completeArchitectDataMock.city,
        region: completeArchitectDataMock.region,
        phone1: completeArchitectDataMock.phone1,
        phone2: completeArchitectDataMock.phone2,
        obs: completeArchitectDataMock.obs,
        sellerId: completeArchitectDataMock.sellerId,
      };

      const useCasesListSpy = jest
        .spyOn(useCases, 'list')
        .mockImplementationOnce(async () => [completeArchitectDataMock]);

      const architectToArchitectsListPresenter = jest
        .spyOn(Architect, 'toArchitectsListPresenter')
        .mockImplementationOnce(() => [expectedResult]);

      // when
      const result = await controller.list();

      // then
      expect(useCasesListSpy).toBeCalled();
      expect(architectToArchitectsListPresenter).toBeCalledWith([
        completeArchitectDataMock,
      ]);
      expect(result).toEqual({
        statusCode: 200,
        payload: [expectedResult],
        success: true,
      });
    });
  });

  describe('upsert', () => {
    it('should return the data of upserted user', async () => {
      // given
      const expectedResult = {
        id: completeArchitectDataMock.id,
        email: completeArchitectDataMock.email,
        name: completeArchitectDataMock.name,
        cpf: completeArchitectDataMock.cpf,
        nasc: completeArchitectDataMock.nasc,
        active: completeArchitectDataMock.active,
        cep: completeArchitectDataMock.cep,
        address: completeArchitectDataMock.address,
        district: completeArchitectDataMock.district,
        city: completeArchitectDataMock.city,
        region: completeArchitectDataMock.region,
        phone1: completeArchitectDataMock.phone1,
        phone2: completeArchitectDataMock.phone2,
        obs: completeArchitectDataMock.obs,
        sellerId: completeArchitectDataMock.sellerId,
      };

      const userUseCasesUpsertSpy = jest
        .spyOn(useCases, 'upsert')
        .mockImplementationOnce(async () => completeArchitectDataMock);

      const architectToUpsertArchitectPresenterSpy = jest
        .spyOn(Architect, 'toUpsertArchitectPresenter')
        .mockImplementationOnce(() => expectedResult);

      // when
      const result = await controller.upsert(
        basicArchitectDataMock as UpsertArchitectDTO,
      );

      // then
      expect(userUseCasesUpsertSpy).toBeCalledWith(
        basicArchitectDataMock as UpsertArchitectDTO,
      );
      expect(architectToUpsertArchitectPresenterSpy).toBeCalledWith(
        completeArchitectDataMock,
      );
      expect(result).toEqual({
        statusCode: 201,
        payload: expectedResult,
        success: true,
      });
    });
  });
});
