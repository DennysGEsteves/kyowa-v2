import { IFormatUpsertUserDBData } from '@core/services/user';
import prisma from '@infra/repositories/prismaClient';
import { IUserRepository } from '@infra/repositories/user';
import { User as DBUser } from '@prisma/client';

const { user: db } = prisma;

export class UserRepository implements IUserRepository {
  list(): Promise<DBUser[]> {
    return db.findMany();
  }

  upsert(user: IFormatUpsertUserDBData): Promise<DBUser> {
    return db.upsert({
      where: { email: user.email },
      create: user,
      update: user,
    });
  }

  findByEmail(email: string): Promise<DBUser> {
    return db.findFirst({
      where: { email },
    });
  }

  remove(id: number): Promise<DBUser> {
    return db.delete({
      where: { id },
    });
  }
}
