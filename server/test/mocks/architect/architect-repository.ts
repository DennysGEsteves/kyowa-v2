import { IArchitectRepository } from '@infra/repositories/architect';

export const ArchitectRepositoryMock = jest.fn().mockImplementation(() => {
  return {
    list: jest.fn(),
    upsert: jest.fn(),
    findByEmail: jest.fn(),
  } as IArchitectRepository;
});
