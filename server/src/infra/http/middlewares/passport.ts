import passportJWT from 'passport-jwt';

import { UserRepository } from '@infra/repositories/user';

const JWTStrategy = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;
const opts = {
  jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
  secretOrKey: process.env.JWT_SECRET,
};
const userRepository = new UserRepository();

export const jWTStrategy = new JWTStrategy(opts, async (jwtPayload, done) => {
  const user = await userRepository.findByEmail(jwtPayload.sub);
  return done(null, user);
});
