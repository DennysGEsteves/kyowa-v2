import { UserPermissions } from '@core/models/user';

export const basicUserDataMock = {
  email: 'someemail@email.com',
  name: 'some name',
  permission: 'admin' as UserPermissions,
  storeId: 1,
};

export const completeUserDataMock = {
  email: 'someemail@email.com',
  name: 'some name',
  permission: 'admin' as UserPermissions,
  storeId: 1,
  phone: 'some phone',
  active: true,
  id: 99999,
  login: 'someemail',
  pass: 'some pass',
};
