import { UpsertStoreDTO } from '@core/controllers/store/dtos';
import { Store as DBStore } from '@prisma/client';

interface IConstructorParams {
  readonly mid?: string;
  readonly name: string;
  readonly id?: number;
  readonly email?: string;
  readonly cep?: string;
  readonly address?: string;
  readonly district?: string;
  readonly city?: string;
  readonly region?: string;
  readonly phone1?: string;
  readonly phone2?: string;
  readonly obs?: string;
  readonly managerId?: number;
}

export class Store {
  public readonly mid?: string;

  public readonly name: string;

  public readonly id?: number;

  public readonly email?: string;

  public readonly cep?: string;

  public readonly address?: string;

  public readonly district?: string;

  public readonly city?: string;

  public readonly region?: string;

  public readonly phone1?: string;

  public readonly phone2?: string;

  public readonly obs?: string;

  public readonly managerId?: number;

  public constructor({
    address,
    cep,
    city,
    district,
    email,
    id,
    managerId,
    name,
    obs,
    phone1,
    phone2,
    region,
  }: IConstructorParams) {
    this.name = name;
    this.email = email;
    this.address = address;
    this.cep = cep;
    this.city = city;
    this.district = district;
    this.id = id;
    this.managerId = managerId;
    this.obs = obs;
    this.phone1 = phone1;
    this.phone2 = phone2;
    this.region = region;
  }

  public static fromStoreListDBResponse(data: DBStore[]): Store[] {
    return data.map(
      (dbStore) =>
        new Store({
          id: dbStore.id,
          name: dbStore.name,
          email: dbStore.email,
          address: dbStore.address,
          cep: dbStore.cep,
          city: dbStore.city,
          district: dbStore.district,
          managerId: dbStore.managerId,
          obs: dbStore.obs,
          phone1: dbStore.phone1,
          phone2: dbStore.phone2,
          region: dbStore.region,
        }),
    );
  }

  public static fromStoreDBResponse(dbStore: DBStore): Store {
    return new Store({
      id: dbStore.id,
      name: dbStore.name,
      email: dbStore.email,
      address: dbStore.address,
      cep: dbStore.cep,
      city: dbStore.city,
      district: dbStore.district,
      managerId: dbStore.managerId,
      obs: dbStore.obs,
      phone1: dbStore.phone1,
      phone2: dbStore.phone2,
      region: dbStore.region,
    });
  }

  public static fromUpsertStoreDTO(dto: UpsertStoreDTO): Store {
    return new Store({
      id: dto.id,
      name: dto.name,
      email: dto.email,
      address: dto.address,
      cep: dto.cep,
      city: dto.city,
      district: dto.district,
      managerId: dto.managerId,
      obs: dto.obs,
      phone1: dto.phone1,
      phone2: dto.phone2,
      region: dto.region,
    });
  }

  public static toStoresListPresenter = (stores: Store[]): Store[] => {
    return stores.map((store) => {
      return {
        id: store.id,
        name: store.name,
        email: store.email,
        address: store.address,
        cep: store.cep,
        city: store.city,
        district: store.district,
        managerId: store.managerId,
        obs: store.obs,
        phone1: store.phone1,
        phone2: store.phone2,
        region: store.region,
      };
    });
  };

  public static toUpsertStorePresenter = (store: Store): Store => {
    return {
      id: store.id,
      name: store.name,
      email: store.email,
      address: store.address,
      cep: store.cep,
      city: store.city,
      district: store.district,
      managerId: store.managerId,
      obs: store.obs,
      phone1: store.phone1,
      phone2: store.phone2,
      region: store.region,
    };
  };
}
