import { UpsertClientDTO } from '@core/controllers/client/dtos';
import { Client as DBClient } from '@prisma/client';

import { ClientInterestProducts, ClientOrigins } from './types';

interface IConstructorParams {
  readonly mid?: string;
  readonly id: number;
  readonly name: string;
  readonly nameFilter: string;
  readonly cpf?: string;
  readonly rg?: string;
  readonly architectID?: number;
  readonly nasc?: Date;
  readonly occupation?: string;
  readonly email?: string;
  readonly cep?: string;
  readonly address?: string;
  readonly district?: string;
  readonly city?: string;
  readonly region?: string;
  readonly phone1?: string;
  readonly phone2?: string;
  readonly obs?: string;
  readonly active?: boolean;
  readonly interestProducts?: ClientInterestProducts[];
  readonly origins?: ClientOrigins[];
  readonly entry: Date;
}

export class Client {
  public readonly mid?: string;

  public readonly id: number;

  public readonly name: string;

  public readonly nameFilter?: string;

  public readonly cpf?: string;

  public readonly rg?: string;

  public readonly architectID?: number;

  public readonly nasc?: Date;

  public readonly occupation?: string;

  public readonly email?: string;

  public readonly cep?: string;

  public readonly address?: string;

  public readonly district?: string;

  public readonly city?: string;

  public readonly region?: string;

  public readonly phone1?: string;

  public readonly phone2?: string;

  public readonly obs?: string;

  public readonly active?: boolean;

  public readonly interestProducts?: ClientInterestProducts[];

  public readonly origins?: ClientOrigins[];

  public readonly entry: Date;

  public constructor({
    id,
    name,
    nameFilter,
    cpf,
    rg,
    architectID,
    nasc,
    occupation,
    email,
    cep,
    address,
    district,
    city,
    region,
    phone1,
    phone2,
    obs,
    active,
    interestProducts,
    origins,
    entry,
  }: IConstructorParams) {
    this.id = id;
    this.name = name;
    this.nameFilter = nameFilter;
    this.cpf = cpf;
    this.rg = rg;
    this.architectID = architectID;
    this.nasc = nasc;
    this.occupation = occupation;
    this.email = email;
    this.cep = cep;
    this.address = address;
    this.district = district;
    this.city = city;
    this.region = region;
    this.phone1 = phone1;
    this.phone2 = phone2;
    this.obs = obs;
    this.active = active;
    this.interestProducts = interestProducts as ClientInterestProducts[];
    this.origins = origins;
    this.entry = entry;
  }

  public static fromClientListDBResponse(data: DBClient[]): Client[] {
    return data.map(
      (dbClient) =>
        new Client({
          id: dbClient.id,
          name: dbClient.name,
          nameFilter: dbClient.nameFilter,
          cpf: dbClient.cpf,
          rg: dbClient.rg,
          architectID: dbClient.architectID,
          nasc: dbClient.nasc,
          occupation: dbClient.occupation,
          email: dbClient.email,
          cep: dbClient.cep,
          address: dbClient.address,
          district: dbClient.district,
          city: dbClient.city,
          region: dbClient.region,
          phone1: dbClient.phone1,
          phone2: dbClient.phone2,
          obs: dbClient.obs,
          active: dbClient.active,
          interestProducts:
            dbClient.interestProducts as ClientInterestProducts[],
          origins: dbClient.origins as ClientOrigins[],
          entry: dbClient.entry,
        }),
    );
  }

  public static fromClientDBResponse(dbClient: DBClient): Client {
    return new Client({
      id: dbClient.id,
      name: dbClient.name,
      nameFilter: dbClient.nameFilter,
      cpf: dbClient.cpf,
      rg: dbClient.rg,
      architectID: dbClient.architectID,
      nasc: dbClient.nasc,
      occupation: dbClient.occupation,
      email: dbClient.email,
      cep: dbClient.cep,
      address: dbClient.address,
      district: dbClient.district,
      city: dbClient.city,
      region: dbClient.region,
      phone1: dbClient.phone1,
      phone2: dbClient.phone2,
      obs: dbClient.obs,
      active: dbClient.active,
      interestProducts: dbClient.interestProducts as ClientInterestProducts[],
      origins: dbClient.origins as ClientOrigins[],
      entry: dbClient.entry,
    });
  }

  public static fromClientUpserDTO(dto: UpsertClientDTO): Client {
    return new Client({
      id: dto.id,
      name: dto.name,
      nameFilter: dto.nameFilter,
      cpf: dto.cpf,
      rg: dto.rg,
      architectID: dto.architectID,
      nasc: dto.nasc,
      occupation: dto.occupation,
      email: dto.email,
      cep: dto.cep,
      address: dto.address,
      district: dto.district,
      city: dto.city,
      region: dto.region,
      phone1: dto.phone1,
      phone2: dto.phone2,
      obs: dto.obs,
      active: dto.active,
      interestProducts: dto.interestProducts,
      origins: dto.origins,
      entry: new Date(),
    });
  }

  public static toClientListPresenter = (clients: Client[]): Client[] => {
    return clients.map((client) => {
      return {
        id: client.id,
        name: client.name,
        nameFilter: client.nameFilter,
        cpf: client.cpf,
        rg: client.rg,
        architectID: client.architectID,
        nasc: client.nasc,
        occupation: client.occupation,
        email: client.email,
        cep: client.cep,
        address: client.address,
        district: client.district,
        city: client.city,
        region: client.region,
        phone1: client.phone1,
        phone2: client.phone2,
        obs: client.obs,
        active: client.active,
        interestProducts: client.interestProducts,
        origins: client.origins,
        entry: client.entry,
      };
    });
  };

  public static toUpsertClientPresenter = (client: Client): Client => {
    return {
      id: client.id,
      name: client.name,
      nameFilter: client.nameFilter,
      cpf: client.cpf,
      rg: client.rg,
      architectID: client.architectID,
      nasc: client.nasc,
      occupation: client.occupation,
      email: client.email,
      cep: client.cep,
      address: client.address,
      district: client.district,
      city: client.city,
      region: client.region,
      phone1: client.phone1,
      phone2: client.phone2,
      obs: client.obs,
      active: client.active,
      interestProducts: client.interestProducts,
      origins: client.origins,
      entry: client.entry,
    };
  };
}
