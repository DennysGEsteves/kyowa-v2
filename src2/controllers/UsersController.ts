import { Request, Response } from 'express';
import { readFileSync } from 'fs';

import * as UsersService from '@services/UsersService';

export const list = async (req: Request, res: Response): Promise<void> => {
  try {
    const users = await UsersService.list();
    res.json(users);
  } catch (e) {
    console.log(e);
    res.status(500).send('Erro');
  }
};

export const create = async (req: Request, res: Response): Promise<void> => {
  try {
    const newUserData = req.body;
    const user = await UsersService.create(newUserData);
    res.json(user);
  } catch (e) {
    console.log(e);
    res.status(500).send('Erro');
  }
};

export const update = async (req: Request, res: Response): Promise<void> => {
  const { userId } = req.params;

  try {
    const user = await UsersService.update(Number(userId), req.body);
    res.json(user);
  } catch (e) {
    console.log(e);
    res.status(500).send('Erro');
  }
};

export const remove = async (req: Request, res: Response): Promise<void> => {
  const { userId } = req.params;

  try {
    await UsersService.remove(Number(userId));
    res.send('ok');
  } catch (e) {
    console.log(e);
    res.status(500).send('Erro');
  }
};

export const getImage = async (req: Request, res: Response): Promise<void> => {
  const { userId } = req.params;
  let img;
  try {
    img = readFileSync(`public/img/users/${userId}.jpg`);
  } catch (error) {
    img = readFileSync('public/img/users/avatar.jpg');
  }

  res.writeHead(200, { 'Content-Type': 'image/jpeg' });
  res.end(img, 'binary');
};
