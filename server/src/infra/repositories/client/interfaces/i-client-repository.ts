import { IFormatUpsertClientDBData } from '@core/services/client/';
import { Client as DBClient } from '@prisma/client';

export interface IClientRepository {
  list: () => Promise<DBClient[]>;
  upsert: (client: IFormatUpsertClientDBData, id: number) => Promise<DBClient>;
  findByName(name: string): Promise<DBClient>;
}
