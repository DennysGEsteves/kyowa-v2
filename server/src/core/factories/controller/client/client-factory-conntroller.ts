import { ClientService } from 'core/services/client';

import { ClientController } from '@core/controllers/client';
import { ClientUseCases } from '@core/use-cases/client';
import { ClientRepository } from '@infra/repositories/client';

export const clientFactoryController = (): ClientController => {
  const repository = new ClientRepository();
  const service = new ClientService(repository);
  const useCases = new ClientUseCases(service);
  const controller = new ClientController(useCases);
  return controller;
};
