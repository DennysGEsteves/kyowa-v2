import bcrypt from 'bcrypt';

import { PrismaClient } from '@prisma/client';

const salt = bcrypt.genSaltSync(10);

const prisma = new PrismaClient();

async function main() {
  await prisma.store.upsert({
    where: { id: 1 },
    update: {},
    create: {
      email: 'loja1@email.com',
      name: 'Loja 1',
      id: 1,
    },
  });

  await prisma.user.upsert({
    where: { email: 'dennysteves@hotmail.com' },
    update: {},
    create: {
      email: 'admin@email.com',
      name: 'admin',
      permission: 'admin',
      pass: bcrypt.hashSync('mudar123', salt),
      id: 1,
      storeId: 1,
    },
  });
}

main()
  .catch((e) => {
    console.error(e);
    // process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
