import { UpsertStoreDTO } from '@core/controllers/store/dtos';
import { Store } from '@core/models/store';
import { IStoreUseCases } from '@core/use-cases/store';
import { IResponseBody, ResponseTypes } from '@infra/http/response-controller';

export class StoreController {
  constructor(private storeUseCases: IStoreUseCases) {}

  async list(): Promise<IResponseBody> {
    const stores = await this.storeUseCases.list();
    return ResponseTypes.ok(Store.toStoresListPresenter(stores));
  }

  async upsert(dto: UpsertStoreDTO): Promise<IResponseBody> {
    const store = await this.storeUseCases.upsert(dto);
    return ResponseTypes.created(Store.toUpsertStorePresenter(store));
  }
}
