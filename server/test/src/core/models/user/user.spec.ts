import { UpsertUserDTO } from '@core/controllers/user/dtos';
import { User } from '@core/models/user';
import { UtilsLogin } from '@core/utils';
import { User as DBUser } from '@prisma/client';

import { basicUserDataMock, completeUserDataMock } from '../../../../mocks';

describe('UserModel', () => {
  it('should return an instance of User with all parameters', () => {
    // given

    // when
    const user = new User(completeUserDataMock);

    // then
    expect(user).toBeInstanceOf(User);
  });

  it('should create an instance with default params only', () => {
    // given
    process.env.DEFAULT_USER_PASS = 'some default pass';

    // when
    const user = new User(basicUserDataMock);

    // then
    expect(user).toBeInstanceOf(User);
  });

  describe('fromUserListDBResponse', () => {
    it('should create an instance of User from database list', () => {
      // given
      const dbUsersListMock = [completeUserDataMock] as DBUser[];

      // when
      const user = User.fromUserListDBResponse(dbUsersListMock);

      // then
      expect(user[0]).toBeInstanceOf(User);
    });
  });

  describe('fromUserDBResponse', () => {
    it('should create an instance of User from database item', () => {
      // given
      const dbUserMock = completeUserDataMock as DBUser;

      // when
      const user = User.fromUserDBResponse(dbUserMock);

      // then
      expect(user).toBeInstanceOf(User);
    });
  });

  describe('fromUpsertUserDTO', () => {
    it('should create an instance of User with generated login and pass', () => {
      // given
      const dtoUserMock = basicUserDataMock as UpsertUserDTO;
      const defaultUserPass = 'some default pass';
      process.env.DEFAULT_USER_PASS = defaultUserPass;
      const generatedLogin = UtilsLogin.genLoginFromEmail(dtoUserMock.email);

      // when
      const user = User.fromUpsertUserDTO(dtoUserMock);

      // then
      expect(user).toBeInstanceOf(User);
      expect(user.login).toEqual(generatedLogin);
      expect(user.pass).toEqual(defaultUserPass);
    });

    it('should create an instance of User from upsert dto', () => {
      // given
      const dtoUserMock = completeUserDataMock as UpsertUserDTO;

      // when
      const user = User.fromUpsertUserDTO(dtoUserMock);

      // then
      expect(user).toBeInstanceOf(User);
    });
  });

  describe('toUsersListPresenter', () => {
    it('should create a presenter user', () => {
      // given
      const userMock = [completeUserDataMock] as User[];

      // when
      const users = User.toUsersListPresenter(userMock);

      // then
      expect(users[0]).toEqual({
        name: userMock[0].name,
        email: userMock[0].email,
        permission: userMock[0].permission,
        storeId: userMock[0].storeId,
        active: userMock[0].active,
        login: userMock[0].login,
        phone: userMock[0].phone,
      });
    });
  });

  describe('toUpsertUserPresenter', () => {
    it('should create a presenter user', () => {
      // given
      const userMock = completeUserDataMock as User;

      // when
      const user = User.toUpsertUserPresenter(userMock);

      // then
      expect(user).toEqual({
        id: userMock.id,
        name: userMock.name,
        email: userMock.email,
        permission: userMock.permission,
        storeId: userMock.storeId,
        active: userMock.active,
        login: userMock.login,
        phone: userMock.phone,
      });
    });
  });

  describe('toLoggerPresenter', () => {
    it('should create a presenter user', () => {
      // given
      const userMock = completeUserDataMock as User;

      // when
      const user = User.toLoggerPresenter(userMock);

      // then
      expect(user).toEqual({
        id: userMock.id,
        name: userMock.name,
        email: userMock.email,
        permission: userMock.permission,
        storeId: userMock.storeId,
        active: userMock.active,
        login: userMock.login,
        phone: userMock.phone,
      });
    });
  });
});
