import { User } from '@core/models/user';
import { User as DBUser } from '@prisma/client';

export interface IUserService {
  list: () => Promise<User[]>;
  upsert(user: User): Promise<User>;
  findByEmail(email: string): Promise<DBUser>;
  remove: (id: number) => Promise<void>;
}
