import { Router } from 'express';

import * as StoresController from '@controllers/StoresController';

const router = Router();

router.get('/', StoresController.list);
router.post('/', StoresController.create);
router.put('/:userId', StoresController.update);
router.delete('/:userId', StoresController.remove);

export default router;
