import { Request, Response, Router } from 'express';

import { AuthenticateUserDTO } from '@core/controllers/auth/dtos';
import { authFactoryController } from '@core/factories/controller/auth';
import { validateDto } from '@infra/http/middlewares/dto-validator';
import { ResponseController } from '@infra/http/response-controller';

class AuthRoutes {
  router = Router();

  private authController = authFactoryController();

  constructor() {
    this.authenticate();
  }

  authenticate() {
    this.router.post(
      '/',
      validateDto(AuthenticateUserDTO),
      async (req: Request, res: Response) => {
        return ResponseController.toJson(
          req,
          res,
          this.authController.authenticate({
            ...req.body,
          }),
        );
      },
    );
  }
}

export default new AuthRoutes().router;
