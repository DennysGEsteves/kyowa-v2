import { Store } from '@core/models/store';

import { IFormatUpsertStoreDBData } from '../interfaces';

export const formatUpsertStoreDBData = (
  store: Store,
): IFormatUpsertStoreDBData => {
  return {
    name: store.name,
    email: store.email,
    address: store.address,
    cep: store.cep,
    city: store.city,
    district: store.district,
    managerId: store.managerId,
    obs: store.obs,
    phone1: store.phone1,
    phone2: store.phone2,
    region: store.region,
  };
};
