import { Request, Response, Router } from 'express';

import { UpsertArchitectDTO } from '@core/controllers/architect/dtos';
import { architectFactoryController } from '@core/factories/controller';
import { validateDto } from '@infra/http/middlewares/dto-validator';
import { ResponseController } from '@infra/http/response-controller';

class ArchitectRoutes {
  router = Router();

  architectController = architectFactoryController();

  constructor() {
    this.list();
    this.upsert();
  }

  list() {
    this.router.get('/', async (req: Request, res: Response) => {
      return ResponseController.toJson(
        req,
        res,
        this.architectController.list(),
      );
    });
  }

  upsert() {
    this.router.post(
      '/',
      validateDto(UpsertArchitectDTO),
      async (req: Request, res: Response) => {
        return ResponseController.toJson(
          req,
          res,
          this.architectController.upsert({ ...req.body }),
        );
      },
    );
  }
}

export default new ArchitectRoutes().router;
