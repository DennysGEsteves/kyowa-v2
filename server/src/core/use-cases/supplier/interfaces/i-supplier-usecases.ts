import { UpsertSupplierDTO } from '@core/controllers/supplier/dtos';
import { Supplier } from '@core/models/supplier';

export interface ISupplierUseCases {
  list: () => Promise<Supplier[]>;
  upsert(dto: UpsertSupplierDTO): Promise<Supplier>;
}
