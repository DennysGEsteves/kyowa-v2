import { IStoreUseCases } from '@core/use-cases/store';

export const StoreUseCasesMock = jest.fn().mockImplementation(() => {
  return {
    list: jest.fn(),
    upsert: jest.fn(),
  } as IStoreUseCases;
});
