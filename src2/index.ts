import cookieParser from 'cookie-parser';
import cors from 'cors';
import express from 'express';

import routes from './routes';

const corsOptions = {
  origin: process.env.CLIENT_URL,
  credentials: true,
};

const app = express();
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ limit: '50mb', extended: true }));
app.use(cors(corsOptions));
app.use(cookieParser());

app.use('/api', routes);

const port = process.env.API_PORT || 3001;

const server = app.listen(port, () => {
  console.log(`Servidor iniciado na porta ${port}`);
});

export default server;
