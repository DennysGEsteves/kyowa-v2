import { UserPermissions } from '@core/models/user/types/permissions';

export type IFormatUpsertUserDBData = {
  name: string;
  email: string;
  permission: UserPermissions;
  storeId: number;
  phone: string;
  active: boolean;
  login: string;
  pass?: string;
};
