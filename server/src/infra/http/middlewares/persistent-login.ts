import { NextFunction, Request, Response } from 'express';
import passport from 'passport';

import { ResponseTypes } from '../response-controller';

export const persistentLogin = (
  req: Request,
  res: Response,
  next: NextFunction,
): any => {
  // Authenicate the cookie sent on the req object.
  passport.authenticate(
    'jwt',
    { session: false },
    async (authErr: any, user: any) => {
      // If there is an system error, send 500 error
      if (authErr)
        return res.json(
          ResponseTypes.fail(
            new Error('Erro ao tentar verificar autenticação'),
          ),
        );

      // If no user is returned, send response showing failure.
      if (!user) {
        return res.json(
          ResponseTypes.fail(
            new Error('Token inválido/usuário não encontrado'),
          ),
        );
      }

      res.locals.user = user;

      return next();
    },
  )(req, res, next);
};
