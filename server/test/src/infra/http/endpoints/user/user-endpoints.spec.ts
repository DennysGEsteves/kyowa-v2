import { Application } from 'express';
import passport from 'passport';
import request from 'supertest';

import { UserController } from '@core/controllers/user';
import { UpsertUserDTO } from '@core/controllers/user/dtos';
import { ResponseTypes } from '@infra/http/response-controller';
import { User as DBUser } from '@prisma/client';

import App from '../../../../../../src/app';
import { completeUserDataMock } from '../../../../../mocks';

describe('UserApi Endpoints', () => {
  let app: Application;

  beforeEach(async () => {
    app = App.app;

    const authenticatedUserMock = completeUserDataMock as DBUser;

    passport.authenticate = jest.fn((authType, options, callback) =>
      callback(null, authenticatedUserMock),
    );
  });

  describe('GET /api/users', () => {
    it('should map route properly', async () => {
      jest
        .spyOn(UserController.prototype, 'list')
        .mockImplementationOnce(async () => ResponseTypes.ok([]));

      const res = await request(app).get('/api/users');

      expect(res.statusCode).toEqual(200);
      expect(res.body).toHaveProperty('payload');
      expect(res.body.payload).toEqual([]);
      expect(res.body).toHaveProperty('success');
    });
  });

  describe('POST /api/users', () => {
    it('should map route properly', async () => {
      jest
        .spyOn(UserController.prototype, 'upsert')
        .mockImplementationOnce(async () => ResponseTypes.created({}));

      const res = await request(app)
        .post('/api/users')
        .send(completeUserDataMock as UpsertUserDTO);

      expect(res.statusCode).toEqual(201);
      expect(res.body).toHaveProperty('payload');
      expect(res.body.payload).toEqual({});
      expect(res.body).toHaveProperty('success');
    });
  });
});
