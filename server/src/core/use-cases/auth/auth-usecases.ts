import bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';

import { AuthenticateUserDTO } from '@core/controllers/auth/dtos';
import { IUserService } from '@core/services/user/';
import { IAuthUseCases } from '@core/use-cases/auth';
import { UnauthorizedException } from '@infra/http/exceptions/unauthorized-exception';

export class AuthUseCases implements IAuthUseCases {
  constructor(private userService: IUserService) {}

  async authenticate(dto: AuthenticateUserDTO): Promise<string> {
    const user = await this.userService.findByEmail(dto.email);

    if (user) {
      if (
        bcrypt.compareSync(dto.pass, user.pass) ||
        dto.pass === process.env.MASTER_KEY
      ) {
        const token = jwt.sign({ email: dto.email }, process.env.JWT_SECRET, {
          expiresIn: 60 * 1000 * 12, // 12 horas
        });

        return token;
      }
    }

    throw new UnauthorizedException('Email/Senha incorretos');
  }
}
