import { IFormatUpsertSupplierDBData } from '@core/services/supplier';
import { Supplier as DBSupplier } from '@prisma/client';

export interface ISupplierRepository {
  list: () => Promise<DBSupplier[]>;
  upsert: (
    supplier: IFormatUpsertSupplierDBData,
    id: number,
  ) => Promise<DBSupplier>;
}
