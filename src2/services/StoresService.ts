import { IStoreCreate } from 'src/types/Stores';

import { Store } from '@prisma/client';
import * as StoresRepository from '@repositories/StoresRepository';

export const list = (): Promise<Store[]> => StoresRepository.list();

export const create = (store: IStoreCreate): Promise<Store> =>
  StoresRepository.create(store);

export const update = (id: number, store: Store): Promise<Store> =>
  StoresRepository.update(id, store);

export const remove = (id: number): Promise<Store> =>
  StoresRepository.remove(id);

export const getByEmail = (email: string): Promise<Store> =>
  StoresRepository.getByEmail(email);
