import { UpsertStoreDTO } from '@core/controllers/store/dtos';
import { Store } from '@core/models/store';
import { IStoreService } from '@core/services/store';
import { IStoreUseCases, StoreUseCases } from '@core/use-cases/store';

import { basicStoreDataMock, StoreServiceMock } from '../../../../mocks';

describe('StoreUseCases', () => {
  let storeUseCases: IStoreUseCases;
  let storeService: IStoreService;

  beforeEach(async () => {
    storeService = new StoreServiceMock();
    storeUseCases = new StoreUseCases(storeService);
  });

  it('should be defined', async () => {
    expect(storeUseCases).toBeDefined();
  });

  describe('list', () => {
    it('should return a Stores list', async () => {
      // given
      const listStoresSpy = jest
        .spyOn(storeService, 'list')
        .mockImplementationOnce(async () => []);

      // when
      await storeUseCases.list();

      // then
      expect(listStoresSpy).toBeCalled();
    });
  });

  describe('upsert', () => {
    it('should return the data of upserted store', async () => {
      // given
      const upsertedStoresSpy = jest
        .spyOn(storeService, 'upsert')
        .mockImplementationOnce(async () => undefined);

      const fromUpsertStoreDTOSpy = jest
        .spyOn(Store, 'fromUpsertStoreDTO')
        .mockImplementationOnce(() => basicStoreDataMock);

      // when
      await storeUseCases.upsert(basicStoreDataMock as UpsertStoreDTO);

      // then
      expect(fromUpsertStoreDTOSpy).toBeCalledWith(basicStoreDataMock);
      expect(upsertedStoresSpy).toBeCalledWith(basicStoreDataMock);
    });
  });
});
