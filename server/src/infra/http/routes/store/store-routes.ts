import { Request, Response, Router } from 'express';

import { UpsertStoreDTO } from '@core/controllers/store/dtos';
import { storeFactoryController } from '@core/factories/controller/store';
import { validateDto } from '@infra/http/middlewares/dto-validator';
import { ResponseController } from '@infra/http/response-controller';

class StoreRoutes {
  router = Router();

  storeController = storeFactoryController();

  constructor() {
    this.list();
    this.upsert();
  }

  list() {
    this.router.get('/', async (req: Request, res: Response) => {
      return ResponseController.toJson(req, res, this.storeController.list());
    });
  }

  upsert() {
    this.router.post(
      '/',
      validateDto(UpsertStoreDTO),
      async (req: Request, res: Response) => {
        return ResponseController.toJson(
         req,
          res,
          this.storeController.upsert({ ...req.body }),
        );
      },
    );
  }
}

export default new StoreRoutes().router;
