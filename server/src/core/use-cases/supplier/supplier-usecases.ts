import { UpsertSupplierDTO } from '@core/controllers/supplier/dtos';
import { Supplier } from '@core/models/supplier';
import { ISupplierService } from '@core/services/supplier';

import { ISupplierUseCases } from './interfaces';

export class SupplierUseCases implements ISupplierUseCases {
  constructor(private supplierService: ISupplierService) {}

  list(): Promise<Supplier[]> {
    return this.supplierService.list();
  }

  upsert(dto: UpsertSupplierDTO): Promise<Supplier> {
    const supplier = Supplier.fromUpsertSupplierDTO(dto);
    return this.supplierService.upsert(supplier);
  }
}
