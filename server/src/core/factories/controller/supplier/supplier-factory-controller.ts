import { SupplierController } from '@core/controllers/supplier';
import { SupplierService } from '@core/services/supplier';
import { SupplierUseCases } from '@core/use-cases/supplier';
import { SupplierRepository } from '@infra/repositories/suppliers';

export const supplierFactoryController = (): SupplierController => {
  const repository = new SupplierRepository();
  const service = new SupplierService(repository);
  const useCases = new SupplierUseCases(service);
  const controller = new SupplierController(useCases);
  return controller;
};
