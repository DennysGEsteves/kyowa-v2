import { User } from '@prisma/client';

export type IUserCreate = Omit<User, 'id' | 'pass'>;
export type IUser = Omit<User, 'pass'> & { image?: string };
