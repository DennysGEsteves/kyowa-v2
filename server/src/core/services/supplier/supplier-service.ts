import { Supplier } from '@core/models/supplier';
import {
  ISupplierService,
  formatUpsertSupplierDBData,
} from '@core/services/supplier';
import { ISupplierRepository } from '@infra/repositories/suppliers';

export class SupplierService implements ISupplierService {
  constructor(private supplierRepository: ISupplierRepository) {}

  async list(): Promise<Supplier[]> {
    const suppliers = await this.supplierRepository.list();
    return Supplier.fromSupplierListDBResponse(suppliers);
  }

  async upsert(supplier: Supplier): Promise<Supplier> {
    const formatSupplier = formatUpsertSupplierDBData(supplier);
    const upsertSupplier = await this.supplierRepository.upsert(
      formatSupplier,
      supplier.id,
    );
    return Supplier.fromSupplierDBResponse(upsertSupplier);
  }
}
