import { IFormatUpsertClientDBData } from '@core/services/client';
import { IClientRepository } from '@infra/repositories/client';
import prisma from '@infra/repositories/prismaClient';
import { Client as DBClient } from '@prisma/client';

const { client: db } = prisma;

export class ClientRepository implements IClientRepository {
  list(): Promise<DBClient[]> {
    return db.findMany();
  }

  upsert(client: IFormatUpsertClientDBData, id: number): Promise<DBClient> {
    return db.upsert({
      where: { id: id || 0 },
      create: client,
      update: client,
    });
  }

  findByName(name: string): Promise<DBClient> {
    return db.findFirst({
      where: { name },
    });
  }
}
