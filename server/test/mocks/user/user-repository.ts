import { IUserRepository } from '@infra/repositories/user';

export const UserRepositoryMock = jest.fn().mockImplementation(() => {
  return {
    findByEmail: jest.fn(),
    list: jest.fn(),
    upsert: jest.fn(),
  } as IUserRepository;
});
