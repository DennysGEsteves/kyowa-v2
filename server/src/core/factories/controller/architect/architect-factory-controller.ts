import { ArchitectController } from '@core/controllers/architect';
import { ArchitectService } from '@core/services/architect';
import { ArchitectUseCases } from '@core/use-cases/architect';
import { ArchitectRepository } from '@infra/repositories/architect';

export const architectFactoryController = (): ArchitectController => {
  const repository = new ArchitectRepository();
  const service = new ArchitectService(repository);
  const useCases = new ArchitectUseCases(service);
  const controller = new ArchitectController(useCases);
  return controller;
};
