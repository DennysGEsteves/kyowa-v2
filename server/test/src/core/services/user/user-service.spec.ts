import { User } from '@core/models/user';
import {
  IUserService,
  UserService,
  formatUpsertUserDBData,
} from '@core/services/user';
import { IUserRepository } from '@infra/repositories/user';
import { User as DBUser } from '@prisma/client';

import { UserRepositoryMock } from '../../../../mocks';

jest.mock('@core/services/user/transformers/format-upsert-user-db-data');

describe('UserService', () => {
  let userService: IUserService;
  let userRepository: IUserRepository;

  beforeEach(async () => {
    userRepository = new UserRepositoryMock();
    userService = new UserService(userRepository);
  });

  it('should be defined', async () => {
    expect(userService).toBeDefined();
  });

  describe('list', () => {
    it('should return an users list', async () => {
      // given
      const userMock = {
        name: 'some name',
        email: 'some email',
      };

      const dbUsersMock = [{ ...userMock } as DBUser];

      const userRepositoryListSpy = jest
        .spyOn(userRepository, 'list')
        .mockImplementationOnce(async () => dbUsersMock);

      const UserFromUserListDBResponseSpy = jest
        .spyOn(User, 'fromUserListDBResponse')
        .mockImplementationOnce(() => dbUsersMock);

      // when
      await userService.list();

      // then
      expect(userRepositoryListSpy).toBeCalled();
      expect(UserFromUserListDBResponseSpy).toBeCalledWith(dbUsersMock);
    });
  });

  describe('upsert', () => {
    it('should return a upserted user', async () => {
      // given
      const userMock = {
        name: 'some name',
        email: 'some email',
      };

      const dbUserMock = { ...userMock } as DBUser;

      const formatUpsertUserDBDataSpy = (
        formatUpsertUserDBData as jest.Mock
      ).mockImplementation(() => userMock);

      const userRepositoryUpsertSpy = jest
        .spyOn(userRepository, 'upsert')
        .mockImplementationOnce(async () => dbUserMock);

      const UserFromUserDBResponseSpy = jest
        .spyOn(User, 'fromUserDBResponse')
        .mockImplementationOnce(() => userMock);

      // when
      await userService.upsert(userMock);

      // then
      expect(userRepositoryUpsertSpy).toBeCalled();
      expect(UserFromUserDBResponseSpy).toBeCalledWith(dbUserMock);
      expect(formatUpsertUserDBDataSpy).toBeCalledWith(userMock);
    });
  });

  describe('findByEmail', () => {
    it('should return a user searching by its email', async () => {
      // given
      const userMock = {
        name: 'some name',
        email: 'some email',
      };

      const dbUserMock = { ...userMock } as DBUser;

      const userRepositoryFindByEmailSpy = jest
        .spyOn(userRepository, 'findByEmail')
        .mockImplementationOnce(async () => dbUserMock);

      // when
      await userService.findByEmail(userMock.email);

      // then
      expect(userRepositoryFindByEmailSpy).toBeCalledWith(userMock.email);
    });
  });
});
