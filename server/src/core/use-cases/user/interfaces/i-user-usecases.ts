import { UpsertUserDTO } from '@core/controllers/user/dtos';
import { User } from '@core/models/user';

export interface IUserUseCases {
  list: () => Promise<User[]>;
  upsert(dto: UpsertUserDTO): Promise<User>;
  remove(id: number): Promise<void>;
}
