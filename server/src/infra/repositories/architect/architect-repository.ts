import { IFormatUpsertArchitectDBData } from '@core/services/architect';
import { IArchitectRepository } from '@infra/repositories/architect';
import prisma from '@infra/repositories/prismaClient';
import { Architect as DBArchitect } from '@prisma/client';

const { architect: db } = prisma;

export class ArchitectRepository implements IArchitectRepository {
  list(): Promise<DBArchitect[]> {
    return db.findMany();
  }

  upsert(
    architect: IFormatUpsertArchitectDBData,
    id: number,
  ): Promise<DBArchitect> {
    return db.upsert({
      where: { id: id || 0 },
      create: architect,
      update: architect,
    });
  }

  findByEmail(email: string): Promise<DBArchitect> {
    return db.findFirst({
      where: { email },
    });
  }
}
