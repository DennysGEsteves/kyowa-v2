import { UpsertArchitectDTO } from '@core/controllers/architect/dtos';
import { Architect } from '@core/models/architect';
import { IArchitectService } from '@core/services/architect';
import {
  IArchitectUseCases,
  ArchitectUseCases,
} from '@core/use-cases/architect';

import {
  basicArchitectDataMock,
  ArchitectServiceMock,
} from '../../../../mocks';

describe('ArchitectUseCases', () => {
  let architectUseCases: IArchitectUseCases;
  let architectService: IArchitectService;

  beforeEach(async () => {
    architectService = new ArchitectServiceMock();
    architectUseCases = new ArchitectUseCases(architectService);
  });

  it('should be defined', async () => {
    expect(architectUseCases).toBeDefined();
  });

  describe('list', () => {
    it('should return a Architects list', async () => {
      // given
      const listArchitectsSpy = jest
        .spyOn(architectService, 'list')
        .mockImplementationOnce(async () => []);

      // when
      await architectUseCases.list();

      // then
      expect(listArchitectsSpy).toBeCalled();
    });
  });

  describe('upsert', () => {
    it('should return the data of upserted architect', async () => {
      // given
      const upsertedArchitectsSpy = jest
        .spyOn(architectService, 'upsert')
        .mockImplementationOnce(async () => undefined);

      const fromUpsertArchitectDTOSpy = jest
        .spyOn(Architect, 'fromUpsertArchitectDTO')
        .mockImplementationOnce(() => basicArchitectDataMock);

      // when
      await architectUseCases.upsert(
        basicArchitectDataMock as UpsertArchitectDTO,
      );

      // then
      expect(fromUpsertArchitectDTOSpy).toBeCalledWith(basicArchitectDataMock);
      expect(upsertedArchitectsSpy).toBeCalledWith(basicArchitectDataMock);
    });
  });
});
