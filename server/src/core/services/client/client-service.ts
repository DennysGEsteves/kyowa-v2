import { Client } from '@core/models/client';
import {
  IClientService,
  formatUpsertClientDBData,
} from '@core/services/client';
import { IClientRepository } from '@infra/repositories/client';

export class ClientService implements IClientService {
  constructor(private clientRepository: IClientRepository) {}

  async list(): Promise<Client[]> {
    const clients = await this.clientRepository.list();
    return Client.fromClientListDBResponse(clients);
  }

  async upsert(client: Client): Promise<Client> {
    const formatClient = formatUpsertClientDBData(client);
    const upsertClient = await this.clientRepository.upsert(
      formatClient,
      client.id,
    );
    return Client.fromClientDBResponse(upsertClient);
  }

  async findByName(name: string): Promise<Client> {
    const client = await this.clientRepository.findByName(name);
    return Client.fromClientDBResponse(client);
  }
}
