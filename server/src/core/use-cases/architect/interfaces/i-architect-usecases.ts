import { UpsertArchitectDTO } from '@core/controllers/architect/dtos';
import { Architect } from '@core/models/architect';

export interface IArchitectUseCases {
  list: () => Promise<Architect[]>;
  upsert(dto: UpsertArchitectDTO): Promise<Architect>;
}
