import {
  ArrayMinSize,
  IsArray,
  IsBoolean,
  IsDate,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

import { ClientInterestProducts, ClientOrigins } from '@core/models/client';
import { IsValidEnum } from '@core/utils';

export class UpsertClientDTO {
  @IsOptional()
  @IsNumber()
  readonly id: number;

  @IsNotEmpty()
  @IsString()
  readonly name: string;

  @IsOptional()
  @IsString()
  readonly nameFilter: string;

  @IsOptional()
  @IsString()
  readonly cpf: string;

  @IsOptional()
  @IsString()
  readonly rg: string;

  @IsOptional()
  @IsNumber()
  readonly architectID: number;

  @IsOptional()
  @IsDate()
  readonly nasc: Date;

  @IsOptional()
  @IsString()
  readonly occupation: string;

  @IsOptional()
  @IsString()
  readonly email: string;

  @IsOptional()
  @IsString()
  readonly cep: string;

  @IsOptional()
  @IsString()
  readonly address: string;

  @IsOptional()
  @IsString()
  readonly district: string;

  @IsOptional()
  @IsString()
  readonly city: string;

  @IsOptional()
  @IsString()
  readonly region: string;

  @IsOptional()
  @IsString()
  readonly phone1: string;

  @IsOptional()
  @IsString()
  readonly phone2: string;

  @IsOptional()
  @IsString()
  readonly obs: string;

  @IsOptional()
  @IsBoolean()
  readonly active: boolean;

  @IsNotEmpty()
  @IsArray()
  @ArrayMinSize(1)
  @IsValidEnum(ClientInterestProducts)
  readonly interestProducts: ClientInterestProducts[];

  @IsNotEmpty()
  @IsArray()
  @ArrayMinSize(1)
  @IsValidEnum(ClientOrigins)
  readonly origins: ClientOrigins[];
}
