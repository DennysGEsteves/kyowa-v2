import { IFormatUpsertStoreDBData } from '@core/services/store';
import prisma from '@infra/repositories/prismaClient';
import { IStoreRepository } from '@infra/repositories/store';
import { Store as DBStore } from '@prisma/client';

const { store: db } = prisma;

export class StoreRepository implements IStoreRepository {
  list(): Promise<DBStore[]> {
    return db.findMany();
  }

  upsert(store: IFormatUpsertStoreDBData, id: number): Promise<DBStore> {
    return db.upsert({
      where: { id: id || 0 },
      create: store,
      update: store,
    });
  }

  findById(id: number): Promise<DBStore> {
    return db.findFirst({
      where: { id },
    });
  }
}
