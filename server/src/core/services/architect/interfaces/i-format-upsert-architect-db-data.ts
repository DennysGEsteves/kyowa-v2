export type IFormatUpsertArchitectDBData = {
  name: string;
  name_filter?: string;
  cpf?: string;
  nasc?: Date;
  email?: string;
  cep?: string;
  address?: string;
  district?: string;
  city?: string;
  region?: string;
  phone1?: string;
  phone2?: string;
  obs?: string;
  active?: boolean;
  sellerId?: number;
};
