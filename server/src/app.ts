import 'reflect-metadata';

import cookieParser from 'cookie-parser';
import cors from 'cors';
import express, { json } from 'express';
import passport from 'passport';

import { jWTStrategy } from '@infra/http/middlewares/passport';
import { Routes } from '@infra/http/routes';

class App {
  public app: express.Application;

  constructor() {
    this.app = express();

    this.middlewares();
    this.routes();
  }

  middlewares() {
    this.app.use(json());
    this.app.use(express.json({ limit: '50mb' }));
    this.app.use(express.urlencoded({ limit: '50mb', extended: true }));
    this.app.use(
      cors({
        origin: process.env.CLIENT_URL,
        credentials: true,
      }),
    );
    this.app.use(cookieParser());
    passport.use(jWTStrategy);
  }

  routes() {
    this.app.use('/api', Routes);
  }

  listen(port: number) {
    this.app.listen(port, () =>
      console.log(`Server running on port "${port}"...`),
    );
  }
}

export default new App();
