import { SupplierTypes } from '@core/models/supplier/types/supplier-types';

export type IFormatUpsertSupplierDBData = {
  id: number;
  name: string;
  nameFilter: string;
  cpnj: string;
  im: string;
  ie: string;
  email: string;
  cep: string;
  address: string;
  district: string;
  city: string;
  region: string;
  phone1: string;
  phone2: string;
  obs: string;
  active: boolean;
  type: SupplierTypes;
};
