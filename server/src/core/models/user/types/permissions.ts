export type UserPermissions =
  | 'admin'
  | 'manager'
  | 'sales'
  | 'operational'
  | 'finance';
