import bcrypt from 'bcrypt';

import { User } from '@core/models/user';

import { IFormatUpsertUserDBData } from '../interfaces';

const salt = bcrypt.genSaltSync(10);

export const formatUpsertUserDBData = (user: User): IFormatUpsertUserDBData => {
  const data = {
    active: user.active,
    email: user.email,
    login: user.login,
    name: user.name,
    permission: user.permission,
    phone: user.phone,
    storeId: user.storeId,
    ...(user.pass ? { pass: bcrypt.hashSync(user.pass, salt) } : {}),
  };

  return data;
};
