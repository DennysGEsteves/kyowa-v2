export type FetchOptions = {
  page?: number;
  perPage?: number;
  search?: string;
};

export type FetchResult = {
  totalRows: number;
  rows: any;
};
