import { Architect } from '@core/models/architect';
import {
  IArchitectService,
  formatUpsertArchitectDBData,
} from '@core/services/architect';
import { IArchitectRepository } from '@infra/repositories/architect';

export class ArchitectService implements IArchitectService {
  constructor(private architectRepository: IArchitectRepository) {}

  async list(): Promise<Architect[]> {
    const architects = await this.architectRepository.list();
    return Architect.fromArchitectListDBResponse(architects);
  }

  async upsert(architect: Architect): Promise<Architect> {
    const formatArchitect = formatUpsertArchitectDBData(architect);
    const upsertArchitect = await this.architectRepository.upsert(
      formatArchitect,
      architect.id,
    );
    return Architect.fromArchitectDBResponse(upsertArchitect);
  }

  async findByEmail(email: string): Promise<Architect> {
    const architect = await this.architectRepository.findByEmail(email);
    return Architect.fromArchitectDBResponse(architect);
  }
}
