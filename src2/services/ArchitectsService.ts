import { IArchitectCreate } from "src/types/Architect";
import { FetchOptions, FetchResult } from "src/types/Table";

import { Architect } from "@prisma/client";
import * as ArchitectRepository from "@repositories/ArchitectRepository";

export const list = async (options: FetchOptions): Promise<FetchResult> => {
  return ArchitectRepository.list(options);
};

export const create = (architect: IArchitectCreate): Promise<Architect> => {
  return ArchitectRepository.create(architect);
};

export const update = (
  id: number,
  architect: Architect
): Promise<Architect> => {
  return ArchitectRepository.update(id, architect);
};

export const remove = (id: number): Promise<Architect> =>
  ArchitectRepository.remove(id);

export const getByEmail = (email: string): Promise<Architect> =>
  ArchitectRepository.getByEmail(email);
