import { IStoreRepository } from '@infra/repositories/store';

export const StoreRepositoryMock = jest.fn().mockImplementation(() => {
  return {
    findById: jest.fn(),
    list: jest.fn(),
    upsert: jest.fn(),
  } as IStoreRepository;
});
