import { Store } from '@core/models/store';
import { IStoreService, StoreService } from '@core/services/store';
import { formatUpsertStoreDBData } from '@core/services/store/transformers/format-upsert-store-db-data';
import { IStoreRepository } from '@infra/repositories/store';
import { Store as DBStore } from '@prisma/client';

import { completeStoreDataMock, StoreRepositoryMock } from '../../../../mocks';

jest.mock('@core/services/store/transformers/format-upsert-store-db-data');

describe('StoreService', () => {
  let storeService: IStoreService;
  let storeRepository: IStoreRepository;

  beforeEach(async () => {
    storeRepository = new StoreRepositoryMock();
    storeService = new StoreService(storeRepository);
  });

  it('should be defined', async () => {
    expect(storeService).toBeDefined();
  });

  describe('list', () => {
    it('should return an stores list', async () => {
      // given
      const storeMock = {
        name: 'some name',
        email: 'some email',
      };

      const dbStoresMock = [{ ...storeMock } as DBStore];

      const storeRepositoryListSpy = jest
        .spyOn(storeRepository, 'list')
        .mockImplementationOnce(async () => dbStoresMock);

      const StoreFromStoreListDBResponseSpy = jest
        .spyOn(Store, 'fromStoreListDBResponse')
        .mockImplementationOnce(() => dbStoresMock);

      // when
      await storeService.list();

      // then
      expect(storeRepositoryListSpy).toBeCalled();
      expect(StoreFromStoreListDBResponseSpy).toBeCalledWith(dbStoresMock);
    });
  });

  describe('upsert', () => {
    it('should return a upserted store', async () => {
      // given
      const storeMock = {
        name: 'some name',
        email: 'some email',
      };

      const dbStoreMock = { ...storeMock } as DBStore;

      const formatUpsertStoreDBDataSpy = (
        formatUpsertStoreDBData as jest.Mock
      ).mockImplementation(() => storeMock);

      const storeRepositoryUpsertSpy = jest
        .spyOn(storeRepository, 'upsert')
        .mockImplementationOnce(async () => dbStoreMock);

      const StoreFromStoreDBResponseSpy = jest
        .spyOn(Store, 'fromStoreDBResponse')
        .mockImplementationOnce(() => storeMock);

      // when
      await storeService.upsert(storeMock);

      // then
      expect(storeRepositoryUpsertSpy).toBeCalled();
      expect(StoreFromStoreDBResponseSpy).toBeCalledWith(dbStoreMock);
      expect(formatUpsertStoreDBDataSpy).toBeCalledWith(storeMock);
    });
  });

  describe('findById', () => {
    it('should return a store searching by its id', async () => {
      // given
      const storeIdMock = 123456789;

      const dbStoreMock = { ...completeStoreDataMock } as DBStore;

      const storeRepositoryFindByEmailSpy = jest
        .spyOn(storeRepository, 'findById')
        .mockImplementationOnce(async () => dbStoreMock);

      // when
      await storeService.findById(storeIdMock);

      // then
      expect(storeRepositoryFindByEmailSpy).toBeCalledWith(storeIdMock);
    });
  });
});
