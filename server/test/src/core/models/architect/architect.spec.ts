import { UpsertArchitectDTO } from '@core/controllers/architect/dtos';
import { Architect } from '@core/models/architect';
import { Architect as DBArchitect } from '@prisma/client';

import {
  basicArchitectDataMock,
  completeArchitectDataMock,
} from '../../../../mocks';

describe('ArchitectModel', () => {
  it('should return an instance of Architect with all parameters', () => {
    // given

    // when
    const architect = new Architect(completeArchitectDataMock);

    // then
    expect(architect).toBeInstanceOf(Architect);
  });

  it('should create an instance with default params only', () => {
    // given

    // when
    const architect = new Architect(basicArchitectDataMock);

    // then
    expect(architect).toBeInstanceOf(Architect);
  });

  describe('fromArchitectListDBResponse', () => {
    it('should create an instance of Architect from database list', () => {
      // given
      const dbArchitectsListMock = [completeArchitectDataMock] as DBArchitect[];

      // when
      const architect =
        Architect.fromArchitectListDBResponse(dbArchitectsListMock);

      // then
      expect(architect[0]).toBeInstanceOf(Architect);
    });
  });

  describe('fromArchitectDBResponse', () => {
    it('should create an instance of Architect from database item', () => {
      // given
      const dbArchitectMock = completeArchitectDataMock as DBArchitect;

      // when
      const architect = Architect.fromArchitectDBResponse(dbArchitectMock);

      // then
      expect(architect).toBeInstanceOf(Architect);
    });
  });

  describe('fromUpsertArchitectDTO', () => {
    it('should create an instance of Architect from upsert dto', () => {
      // given
      const dtoArchitectMock = completeArchitectDataMock as UpsertArchitectDTO;

      // when
      const architect = Architect.fromUpsertArchitectDTO(dtoArchitectMock);

      // then
      expect(architect).toBeInstanceOf(Architect);
    });
  });

  describe('toArchitectsListPresenter', () => {
    it('should create a presenter architect', () => {
      // given
      const architectMock = [completeArchitectDataMock] as Architect[];

      // when
      const architects = Architect.toArchitectsListPresenter(architectMock);

      // then
      expect(architects[0]).toEqual({
        id: architectMock[0].id,
        email: architectMock[0].email,
        name: architectMock[0].name,
        cpf: architectMock[0].cpf,
        nasc: architectMock[0].nasc,
        active: architectMock[0].active,
        cep: architectMock[0].cep,
        address: architectMock[0].address,
        district: architectMock[0].district,
        city: architectMock[0].city,
        region: architectMock[0].region,
        phone1: architectMock[0].phone1,
        phone2: architectMock[0].phone2,
        obs: architectMock[0].obs,
        sellerId: architectMock[0].sellerId,
      });
    });
  });

  describe('toUpsertArchitectPresenter', () => {
    it('should create a presenter architect', () => {
      // given
      const architectMock = completeArchitectDataMock as Architect;

      // when
      const architect = Architect.toUpsertArchitectPresenter(architectMock);

      // then
      expect(architect).toEqual({
        id: architectMock.id,
        email: architectMock.email,
        name: architectMock.name,
        cpf: architectMock.cpf,
        nasc: architectMock.nasc,
        active: architectMock.active,
        cep: architectMock.cep,
        address: architectMock.address,
        district: architectMock.district,
        city: architectMock.city,
        region: architectMock.region,
        phone1: architectMock.phone1,
        phone2: architectMock.phone2,
        obs: architectMock.obs,
        sellerId: architectMock.sellerId,
      });
    });
  });
});
