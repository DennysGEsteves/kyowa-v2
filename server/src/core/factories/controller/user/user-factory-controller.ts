import { UserController } from '@core/controllers/user';
import { UserService } from '@core/services/user/user-service';
import { UserUseCases } from '@core/use-cases/user';
import { UserRepository } from '@infra/repositories/user';

export const userFactoryController = (): UserController => {
  const repository = new UserRepository();
  const service = new UserService(repository);
  const useCases = new UserUseCases(service);
  const controller = new UserController(useCases);
  return controller;
};
