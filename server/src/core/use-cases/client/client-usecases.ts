import { UpsertClientDTO } from '@core/controllers/client/dtos';
import { Client } from '@core/models/client';
import { IClientService } from '@core/services/client';
import { IClientUseCases } from '@core/use-cases/client';

export class ClientUseCases implements IClientUseCases {
  constructor(private clientService: IClientService) {}

  list(): Promise<Client[]> {
    return this.clientService.list();
  }

  upsert(dto: UpsertClientDTO): Promise<Client> {
    const client = Client.fromClientUpserDTO(dto);
    return this.clientService.upsert(client);
  }
}
