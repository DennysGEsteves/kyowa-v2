import { Request, Response, Router } from 'express';

import { UpsertUserDTO } from '@core/controllers/user/dtos';
import { userFactoryController } from '@core/factories/controller';
import { validateDto } from '@infra/http/middlewares/dto-validator';
import { ResponseController } from '@infra/http/response-controller';

class UserRoutes {
  router = Router();

  userController = userFactoryController();

  constructor() {
    this.list();
    this.upsert();
    this.remove();
  }

  list() {
    this.router.get('/', async (req: Request, res: Response) => {
      return ResponseController.toJson(req, res, this.userController.list());
    });
  }

  upsert() {
    this.router.post(
      '/',
      validateDto(UpsertUserDTO),
      async (req: Request, res: Response) => {
        return ResponseController.toJson(
          req,
          res,
          this.userController.upsert({ ...req.body }),
        );
      },
    );
  }

  remove() {
    this.router.delete(
      '/:id',

      async (req: Request, res: Response) => {
        const { id } = req.params;
        return ResponseController.toJson(
          req,
          res,
          this.userController.remove(Number(id)),
        );
      },
    );
  }
}

export default new UserRoutes().router;
